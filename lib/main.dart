import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/redux/store.dart';
import 'package:vera/redux/utils_profile_state/utils_profile_state.dart';
import 'package:vera/screens/shared_components/splash_screen.dart';

import 'package:vera/utils/themes.dart';

import 'screens/home_screen/home.dart';

Future<void> main() async {
  await Redux.init();
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      systemStatusBarContrastEnforced: true,
      systemNavigationBarColor: Colors.transparent,
      systemNavigationBarDividerColor: Colors.transparent,
      systemNavigationBarIconBrightness: Brightness.dark,
      statusBarIconBrightness: Brightness.dark));
  SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge,
      overlays: [SystemUiOverlay.top]);
  runApp(const MyApp());
}

class NavigationService {
  static GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
        store: Redux.store!,
        child: StoreConnector<AppState, UtilsProfileState>(
            converter: (store) => store.state.utilsProfileState,
            builder: (context, state) {
              return Sizer(builder: (context, orientation, deviceType) {
                return MaterialApp(
                    navigatorKey: NavigationService.navigatorKey,
                    title: 'VERA Správa Zdrojů',
                    themeMode: state.themeProvider.themeMode,
                    theme: MyThemes.lightTheme,
                    darkTheme: MyThemes.darkTheme,
                    home: SplashScreen());
                // home: const AddServer());
              });
            }));
  }
}
