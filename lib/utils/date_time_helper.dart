extension DateTimeExtension on DateTime {
  DateTime roundDown({Duration delta = const Duration(minutes: 30)}) {
    if (!(DateTime.now().minute == 0 || DateTime.now().minute == 30)) {
      return DateTime.fromMillisecondsSinceEpoch(millisecondsSinceEpoch -
              millisecondsSinceEpoch % delta.inMilliseconds)
          .add(const Duration(minutes: 30));
    }
    return DateTime.now();
  }
}
