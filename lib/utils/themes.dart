import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class ThemeProvider extends ChangeNotifier {
  ThemeMode themeMode = ThemeMode.system;

  ThemeProvider(this.themeMode);

  bool get isDarkMode {
    if (themeMode == ThemeMode.system) {
      final brightness = SchedulerBinding.instance!.window.platformBrightness;
      return brightness == Brightness.dark;
    } else {
      return themeMode == ThemeMode.dark;
    }
  }
}

class MyThemes {
  // static ThemeData lightTheme = ThemeData(
  //   // primarySwatch: MaterialColor(0xff251E4D, color),
  //   // primarySwatch: Colors.indigo,
  //
  //   scaffoldBackgroundColor: const Color.fromRGBO(232, 230, 239, 1),
  //   primaryColor: const Color.fromRGBO(37, 30, 77, 1),
  //   primaryColorDark: const Color.fromRGBO(112, 112, 112, 1),
  //   primaryColorLight: const Color.fromRGBO(37, 30, 77, 1),
  //   secondaryHeaderColor: Colors.white,
  //   cardColor: const Color.fromRGBO(174, 169, 186, 1),
  //
  //   // colorScheme: const ColorScheme.light(),
  // );

  static ThemeData lightTheme = ThemeData(
    // primarySwatch: MaterialColor(0xff251E4D, color),
    primarySwatch: Colors.indigo,
    scaffoldBackgroundColor: const Color.fromRGBO(232, 230, 239, 1),
    appBarTheme: const AppBarTheme(color: Color.fromRGBO(232, 230, 239, 1)),
    primaryColor: Colors.indigo,
    primaryColorDark: const Color.fromRGBO(112, 112, 112, 1),
    primaryColorLight: Colors.indigo,
    secondaryHeaderColor: Colors.white,
    cardColor: const Color.fromRGBO(174, 169, 186, 1),
  );

  // static ThemeData darkTheme = ThemeData(
  //   // primarySwatch: MaterialColor(0xff251E4D, color),
  //   primarySwatch: Colors.indigo,
  //
  //   scaffoldBackgroundColor: const Color.fromRGBO(18, 18, 18, 1),
  //   primaryColor: const Color.fromRGBO(112, 94, 214, 1),
  //   primaryColorDark: Colors.white,
  //   primaryColorLight: Colors.white,
  //   secondaryHeaderColor: const Color.fromRGBO(40, 40, 40, 0.9),
  //   cardColor: const Color.fromRGBO(40, 40, 40, 1),
  //
  //   // colorScheme: const ColorScheme.dark(),
  //   // primarySwatch: MaterialColor(0x251E4D, color),
  // );

  static ThemeData darkTheme = ThemeData(
    scaffoldBackgroundColor: const Color.fromRGBO(18, 18, 18, 1),
    appBarTheme: const AppBarTheme(color: Color.fromRGBO(18, 18, 18, 1)),
    primaryColor: Colors.indigo,
    primarySwatch: Colors.indigo,
    primaryColorDark: Colors.white,
    primaryColorLight: Colors.white,
    secondaryHeaderColor: const Color.fromRGBO(40, 40, 40, 0.9),
    cardColor: const Color.fromRGBO(40, 40, 40, 1),
    inputDecorationTheme: const InputDecorationTheme(
      focusColor: Colors.orange,
      labelStyle: TextStyle(
        color: Colors.orange,
      ),
      hintStyle: TextStyle(
        color: Colors.indigo,
      ),
      iconColor: Colors.indigo,
      hoverColor: Colors.indigo,
      // filled: true,
    ),
    textSelectionTheme: const TextSelectionThemeData(
        cursorColor: Colors.indigo,
        selectionColor: Colors.indigo,
        selectionHandleColor: Colors.indigo),
    colorScheme: const ColorScheme.dark()
        .copyWith(primary: Colors.indigo, secondary: Colors.indigo),
    // primarySwatch: MaterialColor(0x251E4D, color),
  );
}
