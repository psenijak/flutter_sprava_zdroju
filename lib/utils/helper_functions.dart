import 'package:flutter/material.dart';
import 'package:vera/models/user_login_model.dart';
import 'package:vera/redux/utils_profile_state/utils_profile_actions.dart';
import 'package:vera/screens/add_server_screen/add_server.dart';
import 'package:vera/screens/home_screen/home.dart';
import 'package:vera/screens/sign_in_screen/log_in.dart';
import 'package:vera/utils/local_storage_manager.dart';

class HelperManager {
  static Future<Widget> initSplashLoad(BuildContext context) async {
    initUpdateTheme(await StorageManager.loadTheme());
    String server = await StorageManager.loadServer();
    if (server == "") {
      return const AddServer(
        nextScreen: LogIn(),
      );
    } else {
      setServer(server);
      UserLoginModel? user = await StorageManager.loadUser();
      if (user == null) {
        return const LogIn();
      } else {
        setUser(user);
        if (await fetchIsUsersTokenValid()) {
          return const Home();
        }
        setUser(null);
        return const LogIn();
      }
      return const Home();
    }
  }
}
