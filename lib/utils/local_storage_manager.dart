import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vera/models/user_login_model.dart';
import 'package:vera/models/user_model.dart';
import 'package:vera/models/user_position_model.dart';
import 'package:vera/utils/themes.dart';

class StorageManager {
  static Future<UserLoginModel?> loadUser() async {
    return await SharedPreferences.getInstance()
        .then((prefs) => prefs.getString('user') ?? "")
        .then((userJson) => userJson.isEmpty
            ? null
            : UserLoginModel.fromJson(jsonDecode(userJson)));
  }

  static Future<void> storeUser(UserLoginModel? user) async {
    if (user == null) {
      await SharedPreferences.getInstance()
          .then((prefs) => prefs.remove('user'));
    } else {
      await SharedPreferences.getInstance()
          .then((prefs) => prefs.setString('user', jsonEncode(user.toJson())));
    }
  }

  static Future<String> loadServer() async {
    return await SharedPreferences.getInstance()
        .then((prefs) => prefs.getString('server') ?? "");
  }

  static Future<void> storeServer(String server) async {
    return await SharedPreferences.getInstance()
        .then((prefs) => prefs.setString('server', server));
  }

  static Future<ThemeProvider> loadTheme() async {
    return await SharedPreferences.getInstance()
        .then((prefs) => prefs.getString('theme') ?? "")
        .then((themeJson) => ThemeProvider(themeJson == "Light"
            ? ThemeMode.light
            : themeJson == "Dark"
                ? ThemeMode.dark
                : ThemeMode.system));
  }

  static Future<void> storeTheme(ThemeMode newTheme) async {
    return await SharedPreferences.getInstance()
        .then((prefs) => prefs.setString(
            'theme',
            newTheme == ThemeMode.system
                ? "System"
                : newTheme == ThemeMode.dark
                    ? "Dark"
                    : "Light"));
  }
}
