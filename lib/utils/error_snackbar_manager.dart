import 'package:flutter/material.dart';

import '../main.dart';

class ErrorManager {
  static void showError(String message) {
    ScaffoldMessenger.of(NavigationService.navigatorKey.currentContext!)
        .showSnackBar(SnackBar(
      duration: const Duration(seconds: 3),
      // elevation: 6.0,
      backgroundColor: Colors.red,
      padding: const EdgeInsets.symmetric(vertical: 30),
      content: Text(
        message,
        textAlign: TextAlign.center,
      ),
    ));
  }

  static void showSuccess(String message) {
    ScaffoldMessenger.of(NavigationService.navigatorKey.currentContext!)
        .showSnackBar(SnackBar(
      duration: const Duration(seconds: 3),
      // elevation: 6.0,
      backgroundColor: Colors.green,
      padding: const EdgeInsets.symmetric(vertical: 30),
      content: Text(
        message,
        textAlign: TextAlign.center,
      ),
    ));
  }
}
