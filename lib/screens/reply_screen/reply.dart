import 'package:flutter/material.dart';
import 'package:vera/screens/reply_screen/components/reply_body.dart';

class Reply extends StatefulWidget {
  const Reply({Key? key}) : super(key: key);

  @override
  _ReplyState createState() => _ReplyState();
}

class _ReplyState extends State<Reply> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Potvrzení účasti"),
        centerTitle: true,
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: const ReplyBody(),
    );
  }
}
