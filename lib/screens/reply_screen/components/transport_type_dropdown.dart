import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/screens/shared_components/dropdown_component.dart';

class TransportTypeDropdown extends StatefulWidget {
  final bool isExpanded;
  const TransportTypeDropdown({Key? key, required this.isExpanded})
      : super(key: key);

  @override
  _TransportTypeDropdownState createState() => _TransportTypeDropdownState();
}

class _TransportTypeDropdownState extends State<TransportTypeDropdown> {
  //TODO - load from transport type from dial
  List<String> transportTypes = [
    'ponorka',
    'auto',
    'vlak',
    'vyducholod',
    'tank',
    'plachetnice',
  ];

  List<DropdownMenuItem<String>> getTimeDropdowns() {
    List<DropdownMenuItem<String>> children = [];
    for (var i = 0; i < 5; i++) {
      children.add(DropdownMenuItem(
          child: Text(transportTypes[i], overflow: TextOverflow.ellipsis),
          value: transportTypes[i]));
    }

    return children;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5.w / 2),
      width: 100.w,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Dopravni prostredek',
            style: TextStyle(color: Theme.of(context).primaryColorDark),
          ),
          SizedBox(
            height: 50,
            child: DropdownComponent(
              isIconVisible: widget.isExpanded,
              width: 100.w,
              onChanged: () {},
              list: getTimeDropdowns(),
              defaultValue: transportTypes[0],
            ),
          )
        ],
      ),
    );
  }
}
