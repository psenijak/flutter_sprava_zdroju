import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/models/reply_model.dart';
import 'package:vera/screens/reply_screen/components/activity_dropdown.dart';
import 'package:vera/screens/reply_screen/components/transport_type_dropdown.dart';
import 'package:vera/screens/shared_components/date_element.dart';
import 'package:vera/screens/shared_components/input_field.dart';
import 'package:vera/screens/shared_components/switch_component.dart';
import 'package:vera/screens/shared_components/time_element.dart';
import 'package:vera/screens/shared_components/toggle_buton.dart';

class ReplyBody extends StatefulWidget {
  const ReplyBody({Key? key}) : super(key: key);

  @override
  _ReplyBodyState createState() => _ReplyBodyState();
}

class _ReplyBodyState extends State<ReplyBody> {
  ReplyModel model = ReplyModel.initial();
  final _shortcutController = TextEditingController();
  final FocusNode _shortcutFocusNode = FocusNode();
  final _nameController = TextEditingController();
  final FocusNode _nameFocusNode = FocusNode();
  final _noteController = TextEditingController();
  final FocusNode _noteFocusNode = FocusNode();
  final _confirmController = TextEditingController();
  final FocusNode _confirmFocusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    //TODO - přidat zkratku, jméno + udaje z odpovědi
    setState(() {
      model = model.copyWith(
        shortcut: 'PSE',
      );
    });
  }

  updateResponse(String newResponse) {
    setState(() {
      model = model.copyWith(
        response: newResponse,
      );
    });
  }

  updateNote(String newNote) {
    setState(() {
      model = model.copyWith(
        note: newNote,
      );
    });
  }

  updateConfirm(String newConfirm) {
    setState(() {
      model = model.copyWith(
        needConfirm: newConfirm,
      );
    });
  }

  toggleCreateTrip() {
    setState(() {
      model = model.copyWith(
        createTrip: !model.createTrip,
      );
    });
  }

  toggleRequired() {
    setState(() {
      model = model.copyWith(
        isRequired: !model.isRequired,
      );
    });
  }

  updateResponseDate(DateTime newResponseDate) {
    setState(() {
      model = model.copyWith(
        responseDate: DateFormat('dd-MM-yyyy').format(newResponseDate),
      );
    });
  }

  updateDateSince(DateTime newDate) {
    setState(() {
      model = model.copyWith(
        dateSince: DateFormat('dd-MM-yyyy').format(newDate),
      );
    });
  }

  updateDateTo(DateTime newDate) {
    setState(() {
      model = model.copyWith(
        dateTo: DateFormat('dd-MM-yyyy').format(newDate),
      );
    });
  }

  updateTimeSince(DateTime newTime) {
    setState(() {
      model = model.copyWith(
        timeSince: DateFormat.Hm().format(newTime),
      );
    });
  }

  updateTimeTo(DateTime newTime) {
    setState(() {
      model = model.copyWith(
        timeTo: DateFormat.Hm().format(newTime),
      );
    });
  }

  updateShortcut(String newShortcut) {
    setState(() {
      model = model.copyWith(
        shortcut: newShortcut,
      );
    });
  }

  updateName(String newName) {
    setState(() {
      model = model.copyWith(
        name: newName,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            ToggleButtonComponent(
              maxSize: 5.w,
              onSelect: updateResponse,
              defaultValue: model.response,
              options: const ["Přijmout", "Odmítnout", "Nezávazně"],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: DateComponent(
                  label: "Datum odpovědi:",
                  onChange: updateResponseDate,
                  startDate: DateFormat("dd-MM-yyyy").parse(model.responseDate),
                  isDisabled: false),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Vytvořit pracovní cestu:",
                    style:
                        TextStyle(color: Theme.of(context).primaryColorDark)),
                SizedBox(
                  width: 4.w,
                  height: 4.h,
                ),
                SwitchComponent(
                    onToggle: toggleCreateTrip, defaultValue: model.createTrip),
              ],
            ),
            TransportTypeDropdown(
              isExpanded: model.createTrip,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: InputField(
                  inputController: _shortcutController,
                  focusNode: _shortcutFocusNode,
                  label: "Zkratka:",
                  keyboardType: TextInputType.text,
                  hasMaxLine: true,
                  onChanged: updateShortcut,
                  initialValue: model.shortcut),
            ),
            InputField(
                inputController: _nameController,
                focusNode: _nameFocusNode,
                label: "Jméno:",
                keyboardType: TextInputType.text,
                hasMaxLine: true,
                onChanged: updateName,
                initialValue: model.name),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: DateComponent(
                  label: "Datum od:",
                  onChange: updateDateSince,
                  startDate: DateFormat("dd-MM-yyyy").parse(model.dateSince),
                  isDisabled: false),
            ),
            TimeComponent(
                label: "Čas od:",
                onChange: updateTimeSince,
                startTime: DateFormat.Hm().parse(model.timeSince),
                isWholeDay: false),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: DateComponent(
                  label: "Datum do:",
                  onChange: updateDateTo,
                  startDate: DateFormat("dd-MM-yyyy").parse(model.dateTo),
                  isDisabled: false),
            ),
            TimeComponent(
                label: "Čas do:",
                onChange: updateTimeTo,
                startTime: DateFormat.Hm().parse(model.timeTo),
                isWholeDay: false),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: InputField(
                  initialValue: model.note,
                  onChanged: updateNote,
                  inputController: _noteController,
                  focusNode: _noteFocusNode,
                  label: "Poznámka",
                  keyboardType: TextInputType.multiline,
                  hasMaxLine: false),
            ),
            const ActivityDropdown(
              isExpanded: false,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Povinné:",
                      style:
                          TextStyle(color: Theme.of(context).primaryColorDark)),
                  SizedBox(
                    width: 4.w,
                    height: 4.h,
                  ),
                  SwitchComponent(
                      onToggle: toggleRequired, defaultValue: model.isRequired),
                ],
              ),
            ),
            InputField(
                initialValue: model.needConfirm,
                onChanged: updateConfirm,
                inputController: _confirmController,
                focusNode: _confirmFocusNode,
                label: "Schlálení:",
                keyboardType: TextInputType.multiline,
                hasMaxLine: false),
            SafeArea(
              child: Padding(
                padding: const EdgeInsets.only(top: 15.0),
                child: SizedBox(
                  width: 95.w,
                  child: ElevatedButton(
                    onPressed: () {},
                    child: const Text('Potvrdit'),
                    style: ElevatedButton.styleFrom(
                        primary: Theme.of(context).primaryColor),
                  ),
                ),
              ),
            )
            // Padding(
            //   padding: const EdgeInsets.symmetric(vertical: 15),
            //   child: InputField(
            //       initialValue: event.place,
            //       onChanged: updatePlace,
            //       inputController: _addressController,
            //       focusNode: _addressFocusNode,
            //       label: "Místo",
            //       keyboardType: TextInputType.text,
            //       hasMaxLine: true),
            // ),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.center,
            //   children: [
            //     Text("Celý den:",
            //         style: TextStyle(
            //             color: Theme.of(context).primaryColorDark)),
            //     SizedBox(
            //       width: 4.w,
            //       height: 4.h,
            //     ),
            //     SwitchComponent(
            //         onToggle: toggleWholeDay,
            //         defaultValue: event.isWholeDay),
            //   ],
            // ),
            // TimeComponent(
            //   isWholeDay: event.isWholeDay,
            //   label: "Čas od:",
            //   onChange: updateTimeSince,
            //   startTime: DateFormat.Hm().parse(event.timeSince),
            // ),
            // Padding(
            //   padding: const EdgeInsets.symmetric(vertical: 15),
            //   child: DateComponent(
            //       isDisabled: false,
            //       label: "Datum od:",
            //       onChange: updateDateSince,
            //       startDate:
            //           DateFormat("dd-MM-yyyy").parse(event.dateSince)),
            // ),
            // TimeComponent(
            //   isWholeDay: event.isWholeDay,
            //   label: "Čas do:",
            //   onChange: updateTimeTo,
            //   startTime: DateFormat.Hm().parse(event.timeTo),
            // ),
            // Padding(
            //   padding: const EdgeInsets.symmetric(vertical: 15),
            //   child: DateComponent(
            //       isDisabled: false,
            //       label: "Datum do:",
            //       onChange: updateDateTo,
            //       startDate: DateFormat("dd-MM-yyyy").parse(event.dateTo)),
            // ),
            // const Padding(
            //   padding: EdgeInsets.symmetric(vertical: 15),
            //   child: ProjectDropdown(
            //     isDisabled: false,
            //   ),
            // ),
            // // const HostElement(
            // //   isDisabled: false,
            // // ),
            // Padding(
            //   padding: EdgeInsets.symmetric(horizontal: 3.w),
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //     crossAxisAlignment: CrossAxisAlignment.center,
            //     children: [
            //       Text(
            //         "Notifikace:",
            //         style: TextStyle(
            //             color: Theme.of(context).primaryColorDark),
            //       ),
            //       SwitchComponent(
            //           onToggle: toggleNotification,
            //           defaultValue: event.notification),
            //       SizedBox(
            //         width: 3.w,
            //       ),
            //       Text("Schválit:",
            //           style: TextStyle(
            //               color: Theme.of(context).primaryColorDark)),
            //       SwitchComponent(
            //           onToggle: toggleConfirm, defaultValue: event.confirm),
            //       SizedBox(
            //         width: 3.w,
            //       ),
            //       Text("Cesta:",
            //           style: TextStyle(
            //               color: Theme.of(context).primaryColorDark)),
            //       SwitchComponent(
            //           onToggle: toggleTrip, defaultValue: event.trip),
            //     ],
            //   ),
            // ),
            // Padding(
            //   padding: const EdgeInsets.symmetric(vertical: 15),
            //   child: InputField(
            //       initialValue: event.note,
            //       onChanged: updateNote,
            //       inputController: _noteController,
            //       focusNode: _noteFocusNode,
            //       label: "Poznámka",
            //       keyboardType: TextInputType.multiline,
            //       hasMaxLine: false),
            // ),
          ],
        ),
      ),
    );
  }
}
