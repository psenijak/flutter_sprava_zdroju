import 'package:autocomplete_textfield_ns/autocomplete_textfield_ns.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/screens/shared_components/container_wrapper.dart';

class AutocompleteInput extends StatefulWidget {
  final List<dynamic> suggestions;
  final Function onSelect;
  final String hintText;
  final bool hasTrailing;
  const AutocompleteInput(
      {Key? key,
      required this.suggestions,
      required this.onSelect,
      required this.hintText,
      required this.hasTrailing})
      : super(key: key);

  @override
  _AutocompleteInputState createState() => _AutocompleteInputState();
}

class _AutocompleteInputState extends State<AutocompleteInput> {
  GlobalKey<AutoCompleteTextFieldState<dynamic>> key =
      GlobalKey<AutoCompleteTextFieldState<dynamic>>();

  @override
  Widget build(BuildContext context) {
    return ContainerWrapper(
        child: AutoCompleteTextField<dynamic>(
          decoration: InputDecoration(
              focusedBorder: InputBorder.none,
              border: InputBorder.none,
              hintText: widget.hintText,
              suffixIcon: const Icon(Icons.search)),
          itemSubmitted: (item) => setState(() => widget.onSelect(item)),
          key: key,
          suggestions: widget.suggestions,
          itemBuilder: (context, suggestion) => Container(
            color: Theme.of(context).secondaryHeaderColor,
            child: Padding(
                child: ListTile(
                    title: Text(
                      suggestion.name,
                      style: TextStyle(color: Theme.of(context).primaryColor),
                    ),
                    trailing: widget.hasTrailing
                        ? Text(
                            suggestion.shortcut,
                            style: TextStyle(
                                color: Theme.of(context).primaryColorDark),
                          )
                        : null),
                padding: const EdgeInsets.all(8.0)),
          ),
          itemSorter: (a, b) =>
              a.name.toLowerCase().compareTo(b.name.toLowerCase()),
          itemFilter: (suggestion, input) =>
              removeDiacritics(suggestion.name.toLowerCase())
                  .startsWith(removeDiacritics(input.toLowerCase())),
        ),
        width: 95.w,
        outerPadding: EdgeInsets.only(left: 5.w / 2, right: 5.w / 2, top: 10),
        innerPadding: EdgeInsets.symmetric(vertical: 8, horizontal: 5.w));
  }

  String removeDiacritics(String str) {
    var withDia =
        'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽžěĚčČřŘůŮ';
    var withoutDia =
        'AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZzeEcCrRuU';

    for (int i = 0; i < withDia.length; i++) {
      str = str.replaceAll(withDia[i], withoutDia[i]);
    }

    return str;
  }
}
