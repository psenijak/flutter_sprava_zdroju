import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/screens/shared_components/animated_text_button.dart';

class TimeComponent extends StatefulWidget {
  final String label;
  final Function onChange;
  final DateTime startTime;
  final bool isWholeDay;
  const TimeComponent(
      {Key? key,
      required this.label,
      required this.onChange,
      required this.startTime,
      required this.isWholeDay})
      : super(key: key);

  @override
  _TimeComponentState createState() => _TimeComponentState();
}

class _TimeComponentState extends State<TimeComponent> {
  // List<DropdownMenuItem<TimeOfDay>> getTimeDropdowns(TimeOfDay time) {
  //   List<DropdownMenuItem<TimeOfDay>> children = [];
  //   TimeOfDay helperTime = time;
  //   for (var i = 0; i < 5; i++) {
  //     children.add(DropdownMenuItem(
  //         child: Text('${helperTime.hour} : ${helperTime.minute}',
  //             overflow: TextOverflow.ellipsis),
  //         value: helperTime));
  //     helperTime = addThirtyMinutes(helperTime);
  //   }
  //
  //   return children;
  // }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay? pickedS = await showTimePicker(
        context: context,
        initialTime: TimeOfDay(
            hour: widget.startTime.hour, minute: widget.startTime.minute),
        builder: (BuildContext context, Widget? child) {
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
            child: child!,
          );
        });

    if (pickedS != null &&
        pickedS !=
            TimeOfDay(
                hour: widget.startTime.hour, minute: widget.startTime.minute)) {
      widget
          .onChange(DateFormat.Hm().parse('${pickedS.hour}:${pickedS.minute}'));
    }
  }

  TimeOfDay addThirtyMinutes(TimeOfDay startTimeOfDay) {
    DateTime today = DateTime.now();
    DateTime customDateTime = DateTime(today.year, today.month, today.day,
        startTimeOfDay.hour, startTimeOfDay.minute);
    return TimeOfDay.fromDateTime(
        customDateTime.add(const Duration(minutes: 30)));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5.w / 2),
      width: 100.w,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AnimatedSize(
            duration: const Duration(milliseconds: 500),
            curve: Curves.easeIn,
            child: Text(
              widget.label,
              style: TextStyle(
                color: Theme.of(context).primaryColorDark,
                fontSize: widget.isWholeDay ? 0 : 14,
              ),
            ),
          ),
          AnimatedContainer(
            curve: Curves.easeIn,
            duration: const Duration(milliseconds: 500),
            height: widget.isWholeDay ? 0 : 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
                SizedBox(
                  width: 79.w,
                  child: Row(
                    children: [
                      SizedBox(
                        height: 50,
                        width: 50,
                        child: AnimatedTextButton(
                            iconData: Icons.arrow_back_ios,
                            setPadding: true,
                            onChange: () {
                              widget.onChange(widget.startTime
                                  .subtract(const Duration(minutes: 30)));
                            },
                            isClosed: widget.isWholeDay),
                      ),
                      SizedBox(
                        width: 1.w,
                      ),
                      Expanded(
                        child: Container(
                          height: 50,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color:
                                  Theme.of(context).cardColor.withOpacity(0.5),
                              border: Border.all(
                                color: Colors.transparent,
                              ),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(4))),
                          child: Center(
                              child: Text(
                            DateFormat.Hm().format(widget.startTime),
                            style: TextStyle(
                                fontSize: 16,
                                color: Theme.of(context).primaryColorDark),
                          )),
                        ),
                      ),
                      SizedBox(
                        width: 1.w,
                      ),
                      AnimatedTextButton(
                          iconData: Icons.arrow_forward_ios,
                          onChange: () {
                            widget.onChange(widget.startTime
                                .add(const Duration(minutes: 30)));
                          },
                          isClosed: widget.isWholeDay),
                    ],
                  ),
                ),
                SizedBox(
                  height: 50,
                  width: 50,
                  child: TextButton(
                    child: AnimatedSize(
                      duration: const Duration(milliseconds: 500),
                      curve: Curves.easeIn,
                      child: Icon(
                        Icons.access_time,
                        color: Theme.of(context).primaryColorLight,
                        size: widget.isWholeDay ? 0 : 27,
                      ),
                    ),
                    onPressed: () {
                      _selectTime(context);
                    },
                    style: TextButton.styleFrom(
                        backgroundColor:
                            Theme.of(context).cardColor.withOpacity(0.5),
                        primary: Theme.of(context).primaryColorLight),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
