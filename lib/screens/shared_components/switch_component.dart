import 'package:flutter/material.dart';

class SwitchComponent extends StatefulWidget {
  final Function onToggle;
  final bool defaultValue;
  const SwitchComponent(
      {Key? key, required this.onToggle, required this.defaultValue})
      : super(key: key);

  @override
  _SwitchComponentState createState() => _SwitchComponentState();
}

class _SwitchComponentState extends State<SwitchComponent> {
  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: const BoxConstraints(maxHeight: 25, maxWidth: 35),
      child: Switch(
        onChanged: (_) {
          widget.onToggle();
        },
        value: widget.defaultValue,
        activeColor: Theme.of(context).primaryColor,
        activeTrackColor: Theme.of(context).primaryColor.withOpacity(0.4),
      ),
    );
  }
}
