import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class ToggleButtonComponent extends StatefulWidget {
  final List<String> options;
  final String defaultValue;
  final Function onSelect;
  final double maxSize;
  const ToggleButtonComponent(
      {Key? key,
      required this.options,
      required this.defaultValue,
      required this.onSelect,
      required this.maxSize})
      : super(key: key);

  @override
  _ToggleButtonComponentState createState() => _ToggleButtonComponentState();
}

class _ToggleButtonComponentState extends State<ToggleButtonComponent> {
  List<bool> selections = [];

  @override
  void initState() {
    super.initState();

    widget.options.asMap().forEach((index, value) =>
        selections.add(value == widget.defaultValue ? true : false));
  }

  List<Widget> getChildren(BuildContext context) {
    List<Widget> helper = [];
    for (var option in widget.options) {
      helper.add(Container(
        width: _buttonWidth(context),
        alignment: Alignment.center,
        child: Text(option),
      ));
    }
    return helper;
  }

  double _buttonWidth(BuildContext context) {
    final maxWidth = 90.w;
    final buttonCount = widget.options.length;

    final width =
        (MediaQuery.of(context).size.width - widget.maxSize - 4) / buttonCount;
    if (width < maxWidth) {
      return width;
    } else {
      return maxWidth;
    }
  }

  @override
  Widget build(BuildContext context) {
    return ToggleButtons(
      borderColor: Theme.of(context).primaryColorDark.withOpacity(0.60),
      color: Theme.of(context).primaryColorDark.withOpacity(0.60),
      selectedColor: Theme.of(context).primaryColor,
      selectedBorderColor: Theme.of(context).primaryColor,
      fillColor: Theme.of(context).primaryColor.withOpacity(0.08),
      splashColor: Theme.of(context).primaryColor.withOpacity(0.12),
      hoverColor: Theme.of(context).primaryColor.withOpacity(0.04),
      borderRadius: BorderRadius.circular(4.0),
      constraints: const BoxConstraints(minHeight: 36.0),
      isSelected: selections,
      onPressed: (newValue) {
        widget.onSelect(widget.options[newValue]);
        setState(() {
          selections.asMap().forEach((index, value) =>
              {selections[index] = (index == newValue ? true : false)});
          // selections[index] = !selections[index];
        });
      },
      children: getChildren(context),
    );
  }
}
