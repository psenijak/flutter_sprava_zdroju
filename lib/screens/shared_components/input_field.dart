import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class InputField extends StatefulWidget {
  final TextEditingController inputController;
  final FocusNode focusNode;
  final String label;
  final TextInputType keyboardType;
  final bool hasMaxLine;
  final Function onChanged;
  final String initialValue;
  const InputField(
      {Key? key,
      required this.inputController,
      required this.focusNode,
      required this.label,
      required this.keyboardType,
      required this.hasMaxLine,
      required this.onChanged,
      required this.initialValue})
      : super(key: key);

  @override
  _NameFiledState createState() => _NameFiledState();
}

class _NameFiledState extends State<InputField> {
  @override
  void initState() {
    super.initState();
    if (widget.initialValue != '') {
      widget.inputController.text = widget.initialValue;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5.w / 2),
      width: 100.w,
      child: TextFormField(
        onChanged: (newValue) => widget.onChanged(newValue),
        controller: widget.inputController,
        keyboardType: widget.keyboardType,
        focusNode: widget.focusNode,
        maxLines: widget.hasMaxLine ? 1 : null,
        minLines: widget.hasMaxLine ? 1 : 4,
        onTap: () {
          _requestFocus(widget.focusNode);
        },
        cursorColor: Theme.of(context).primaryColor,
        decoration: InputDecoration(
          filled: true,
          border: InputBorder.none,
          focusedBorder: InputBorder.none,
          enabledBorder: InputBorder.none,
          errorBorder: InputBorder.none,
          disabledBorder: InputBorder.none,
          fillColor: Theme.of(context).cardColor.withOpacity(0.5),
          contentPadding:
              const EdgeInsets.only(left: 14.0, bottom: 8.0, top: 8.0),
          labelText: widget.label,
          labelStyle: TextStyle(
              color: widget.focusNode.hasFocus
                  ? Theme.of(context).primaryColor
                  : Theme.of(context).primaryColorDark),
        ),
      ),
    );
  }

  void _requestFocus(FocusNode node) {
    setState(() {
      FocusScope.of(context).requestFocus(node);
    });
  }
}
