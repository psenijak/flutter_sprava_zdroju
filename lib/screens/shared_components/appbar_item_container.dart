import 'package:flutter/material.dart';
import 'package:vera/screens/shared_components/container_wrapper.dart';

class AppBarContainer extends StatelessWidget {
  final double containerWidth;
  final Widget child;
  final VoidCallback onTap;
  const AppBarContainer(
      {this.containerWidth = 50,
      required this.child,
      required this.onTap,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: onTap,
        child: ContainerWrapper(
          child: child,
          height: 50,
          width: containerWidth,
        ));
  }
}
