import 'package:flutter/material.dart';

class AnimatedTextButton extends StatelessWidget {
  final IconData iconData;
  final VoidCallback onChange;
  final bool isClosed;
  final bool setPadding;
  const AnimatedTextButton(
      {Key? key,
      required this.iconData,
      required this.onChange,
      required this.isClosed,
      this.setPadding = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      width: 50,
      child: TextButton(
        child: AnimatedSize(
          curve: Curves.easeIn,
          duration: const Duration(milliseconds: 500),
          child: Padding(
            padding:
                setPadding ? const EdgeInsets.only(left: 8.0) : EdgeInsets.zero,
            child: Icon(
              iconData,
              size: isClosed ? 0 : 27,
              color: Theme.of(context).primaryColorLight,
            ),
          ),
        ),
        onPressed: () {
          onChange();
          // widget.onChange(
          //     widget.startDate.add(const Duration(days: 1)));
        },
        style: TextButton.styleFrom(
            backgroundColor: Theme.of(context).cardColor.withOpacity(0.5),
            primary: Theme.of(context).primaryColorLight),
      ),
    );
  }
}
