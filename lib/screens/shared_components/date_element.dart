import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/screens/shared_components/animated_text_button.dart';

class DateComponent extends StatefulWidget {
  final String label;
  final Function onChange;
  final DateTime startDate;
  final bool isDisabled;
  const DateComponent(
      {Key? key,
      required this.label,
      required this.onChange,
      required this.startDate,
      required this.isDisabled})
      : super(key: key);

  @override
  _DateComponentState createState() => _DateComponentState();
}

class _DateComponentState extends State<DateComponent> {
  // List<DropdownMenuItem<DateTime>> getTimeDropdowns(DateTime date) {
  //   DateTime helperDate = date;
  //   List<DropdownMenuItem<DateTime>> children = [];
  //   for (var i = 0; i < 5; i++) {
  //     children.add(DropdownMenuItem(
  //         child: Text(DateFormat('dd-MM-yyyy').format(helperDate)),
  //         value: helperDate));
  //     helperDate = helperDate.add(const Duration(days: 1));
  //   }
  //   return children;
  // }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
        context: context,
        initialDate: widget.startDate,
        firstDate: DateTime.now(),
        lastDate: DateTime(2101));
    if (picked != null && picked != widget.startDate) {
      widget.onChange(picked);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5.w / 2),
      width: 100.w,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            widget.label,
            style: TextStyle(color: Theme.of(context).primaryColorDark),
          ),
          SizedBox(
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
                SizedBox(
                  width: 79.w,
                  child: Row(
                    children: [
                      AnimatedTextButton(
                          iconData: Icons.arrow_back_ios,
                          setPadding: true,
                          onChange: () {
                            widget.onChange(widget.startDate
                                .subtract(const Duration(days: 1)));
                          },
                          isClosed: false),
                      SizedBox(
                        width: 1.w,
                      ),
                      Expanded(
                        child: Container(
                          height: 50,
                          width: double.infinity,
                          decoration: BoxDecoration(
                              color:
                                  Theme.of(context).cardColor.withOpacity(0.5),
                              border: Border.all(
                                color: Colors.transparent,
                              ),
                              borderRadius:
                                  const BorderRadius.all(Radius.circular(4))),
                          child: Center(
                              child: Text(
                            DateFormat('dd-MM-yyyy').format(widget.startDate),
                            style: TextStyle(
                                fontSize: 16,
                                color: Theme.of(context).primaryColorDark),
                          )),
                        ),
                      ),
                      SizedBox(
                        width: 1.w,
                      ),
                      AnimatedTextButton(
                          iconData: Icons.arrow_forward_ios,
                          onChange: () {
                            widget.onChange(
                                widget.startDate.add(const Duration(days: 1)));
                          },
                          isClosed: false),
                      // SizedBox(
                      //   height: 50,
                      //   width: 50,
                      //   child: TextButton(
                      //     child: Icon(
                      //       Icons.arrow_forward_ios,
                      //       color: Theme.of(context).primaryColorLight,
                      //     ),
                      //     onPressed: ,
                      //     style: TextButton.styleFrom(
                      //         backgroundColor:
                      //             Theme.of(context).cardColor.withOpacity(0.5),
                      //         primary: Theme.of(context).primaryColorLight),
                      //   ),
                      // ),
                    ],
                  ),
                ),
                // DropdownComponent(
                //   onChanged: widget.onChange,
                //   list: getTimeDropdowns(
                //       DateFormat("dd-MM-yyyy").parse(widget.selected)),
                //   defaultValue: DateFormat("dd-MM-yyyy").parse(widget.selected),
                //   width: 79.w,
                //   isIconVisible: true,
                // ),
                SizedBox(
                  height: 50,
                  width: 50,
                  child: TextButton(
                    child: Icon(
                      Icons.calendar_today,
                      color: Theme.of(context).primaryColorLight,
                    ),
                    onPressed: () {
                      _selectDate(context);
                    },
                    style: TextButton.styleFrom(
                        backgroundColor:
                            Theme.of(context).cardColor.withOpacity(0.5),
                        primary: Theme.of(context).primaryColorLight),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
