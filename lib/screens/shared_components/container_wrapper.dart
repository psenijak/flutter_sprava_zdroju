import 'package:flutter/material.dart';

class ContainerWrapper extends StatelessWidget {
  final Widget child;
  final double? width;
  final double? height;
  final EdgeInsets outerPadding;
  final EdgeInsets innerPadding;
  const ContainerWrapper({
    Key? key,
    required this.child,
    this.width,
    this.height,
    this.outerPadding = EdgeInsets.zero,
    this.innerPadding = EdgeInsets.zero,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: outerPadding,
      child: Ink(
        padding: innerPadding,
        decoration: BoxDecoration(
            boxShadow: MediaQuery.of(context).platformBrightness !=
                    Brightness.dark
                ? [
                    BoxShadow(
                      color: Theme.of(context).cardColor,
                      spreadRadius: 3,
                      blurRadius: 7,
                      offset: const Offset(1, 2), // changes position of shadow
                    ),
                  ]
                : [],
            color: Theme.of(context).secondaryHeaderColor,
            borderRadius: const BorderRadius.all(Radius.circular(14))),
        width: width,
        child: child,
        height: height,
      ),
    );
  }
}
