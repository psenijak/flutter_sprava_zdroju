import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class DropdownComponent extends StatefulWidget {
  final List<DropdownMenuItem<dynamic>> list;
  final Function onChanged;
  final dynamic defaultValue;
  final double width;
  final bool isIconVisible;

  const DropdownComponent(
      {Key? key,
      required this.list,
      required this.onChanged,
      required this.defaultValue,
      required this.width,
      required this.isIconVisible})
      : super(key: key);

  @override
  _DropdownComponentState createState() => _DropdownComponentState();
}

class _DropdownComponentState extends State<DropdownComponent> {
  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 500),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
          color: widget.isIconVisible
              ? Theme.of(context).cardColor.withOpacity(0.5)
              : Theme.of(context).cardColor.withOpacity(0.2)),
      width: widget.width,
      child: IgnorePointer(
        ignoring: !widget.isIconVisible,
        child: DropdownButtonHideUnderline(
          child: DropdownButton<dynamic>(
            items: widget.list,
            value: widget.defaultValue,
            onChanged: (value) {
              //TODO - dodelat zmenu stavu
              widget.onChanged(value);
            },
            elevation: 8,
            style: TextStyle(
                color: widget.isIconVisible
                    ? Theme.of(context).primaryColorDark
                    : Theme.of(context).primaryColorDark.withOpacity(0.5),
                fontSize: 16),
            dropdownColor: Theme.of(context).cardColor,
            icon: AnimatedSize(
              duration: const Duration(milliseconds: 500),
              curve: Curves.easeIn,
              child: Icon(
                Icons.keyboard_arrow_down,
                size: widget.isIconVisible ? 22 : 0,
                color: Theme.of(context).primaryColorDark,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
