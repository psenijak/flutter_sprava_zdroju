import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:intl/intl.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/models/event_detail_model.dart';
import 'package:vera/redux/events_state/events_actions.dart';
import 'package:vera/redux/store.dart';
import 'package:vera/screens/shared_components/date_element.dart';
import 'package:vera/screens/create_event_screen/components/descriptions/components/project_dropdown.dart';
import 'package:vera/screens/shared_components/input_field.dart';
import 'package:vera/screens/shared_components/switch_component.dart';
import 'package:vera/screens/shared_components/time_element.dart';
import 'package:vera/screens/shared_components/toggle_buton.dart';

class NewEventDescriptions extends StatefulWidget {
  const NewEventDescriptions({Key? key}) : super(key: key);

  @override
  _NewEventDescriptionsState createState() => _NewEventDescriptionsState();
}

class _NewEventDescriptionsState extends State<NewEventDescriptions> {
  final _titleController = TextEditingController();
  final FocusNode _titleFocusNode = FocusNode();
  final _addressController = TextEditingController();
  final FocusNode _addressFocusNode = FocusNode();
  final _noteController = TextEditingController();
  final FocusNode _noteFocusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, EventDetailModel>(
      converter: (store) => store.state.eventsState.newEvent!,
      builder: (context, event) {
        return SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                InputField(
                    onChanged: updateTitle,
                    initialValue: event.title,
                    inputController: _titleController,
                    focusNode: _titleFocusNode,
                    label: "Název události",
                    keyboardType: TextInputType.text,
                    hasMaxLine: true),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 15),
                  child: InputField(
                      initialValue: event.place,
                      onChanged: updatePlace,
                      inputController: _addressController,
                      focusNode: _addressFocusNode,
                      label: "Místo",
                      keyboardType: TextInputType.text,
                      hasMaxLine: true),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Celý den:",
                        style: TextStyle(
                            color: Theme.of(context).primaryColorDark)),
                    SizedBox(
                      width: 4.w,
                      height: 4.h,
                    ),
                    SwitchComponent(
                        onToggle: toggleWholeDay,
                        defaultValue: event.isWholeDay),
                  ],
                ),
                TimeComponent(
                  isWholeDay: event.isWholeDay,
                  label: "Čas od:",
                  onChange: updateTimeSince,
                  startTime: DateFormat.Hm().parse(event.timeSince),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 15),
                  child: DateComponent(
                      isDisabled: false,
                      label: "Datum od:",
                      onChange: updateDateSince,
                      startDate:
                          DateFormat("dd-MM-yyyy").parse(event.dateSince)),
                ),
                TimeComponent(
                  isWholeDay: event.isWholeDay,
                  label: "Čas do:",
                  onChange: updateTimeTo,
                  startTime: DateFormat.Hm().parse(event.timeTo),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 15),
                  child: DateComponent(
                      isDisabled: false,
                      label: "Datum do:",
                      onChange: updateDateTo,
                      startDate: DateFormat("dd-MM-yyyy").parse(event.dateTo)),
                ),
                ToggleButtonComponent(
                  maxSize: 5.w,
                  onSelect: updateEventType,
                  defaultValue: event.eventType,
                  options: const ["Předběžná", "Závazná", "Soukromá"],
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 15),
                  child: ProjectDropdown(
                    isDisabled: false,
                  ),
                ),
                // const HostElement(
                //   isDisabled: false,
                // ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 3.w),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Notifikace:",
                        style: TextStyle(
                            color: Theme.of(context).primaryColorDark),
                      ),
                      SwitchComponent(
                          onToggle: toggleNotification,
                          defaultValue: event.notification),
                      SizedBox(
                        width: 3.w,
                      ),
                      Text("Schválit:",
                          style: TextStyle(
                              color: Theme.of(context).primaryColorDark)),
                      SwitchComponent(
                          onToggle: toggleConfirm, defaultValue: event.confirm),
                      SizedBox(
                        width: 3.w,
                      ),
                      Text("Cesta:",
                          style: TextStyle(
                              color: Theme.of(context).primaryColorDark)),
                      SwitchComponent(
                          onToggle: toggleTrip, defaultValue: event.trip),
                    ],
                  ),
                ),
                SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 15),
                    child: InputField(
                        initialValue: event.note,
                        onChanged: updateNote,
                        inputController: _noteController,
                        focusNode: _noteFocusNode,
                        label: "Poznámka",
                        keyboardType: TextInputType.multiline,
                        hasMaxLine: false),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
