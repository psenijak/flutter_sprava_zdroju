import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../../../shared_components/dropdown_component.dart';

class ProjectDropdown extends StatefulWidget {
  final bool isDisabled;
  const ProjectDropdown({Key? key, required this.isDisabled}) : super(key: key);

  @override
  _ProjectDropdownState createState() => _ProjectDropdownState();
}

class _ProjectDropdownState extends State<ProjectDropdown> {
  //TODO - load options from redux
  List<DropdownMenuItem<String>> getTimeDropdowns() {
    List<DropdownMenuItem<String>> children = [];
    for (var i = 0; i < 5; i++) {
      children.add(DropdownMenuItem(
          child: Text('VERA-80-00100$i', overflow: TextOverflow.ellipsis),
          value: 'VERA-80-00100$i'));
    }

    return children;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5.w / 2),
      width: 100.w,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Projekt",
            style: TextStyle(color: Theme.of(context).primaryColorDark),
          ),
          SizedBox(
            height: 50,
            child: DropdownComponent(
              isIconVisible: true,
              width: 100.w,
              onChanged: () {},
              list: getTimeDropdowns(),
              defaultValue: 'VERA-80-001000',
            ),
          )
        ],
      ),
    );
  }
}
