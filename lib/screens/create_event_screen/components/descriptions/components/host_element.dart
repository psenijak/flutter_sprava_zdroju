import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/screens/shared_components/dropdown_component.dart';

class HostElement extends StatefulWidget {
  final bool isDisabled;
  const HostElement({Key? key, required this.isDisabled}) : super(key: key);

  @override
  _HostElementState createState() => _HostElementState();
}

class _HostElementState extends State<HostElement> {
  //TODO - load options from redux
  List<DropdownMenuItem<String>> getTimeDropdowns() {
    List<DropdownMenuItem<String>> children = [];
    for (var i = 0; i < 5; i++) {
      children.add(DropdownMenuItem(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('PSE $i',
                  style: TextStyle(color: Theme.of(context).primaryColorLight),
                  overflow: TextOverflow.ellipsis),
              Text('Pšenička Jiří, Ing$i',
                  style: const TextStyle(fontSize: 12),
                  overflow: TextOverflow.ellipsis)
            ],
          ),
          value: 'PSE $i'));
    }

    return children;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5.w / 2),
      width: 100.w,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Organizátor",
            style: TextStyle(color: Theme.of(context).primaryColorDark),
          ),
          SizedBox(
            height: 50,
            child: DropdownComponent(
              isIconVisible: true,
              width: 100.w,
              onChanged: () {},
              list: getTimeDropdowns(),
              defaultValue: 'PSE 0',
            ),
          )
        ],
      ),
    );
  }
}
