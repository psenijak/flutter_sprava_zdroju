import 'package:flutter/material.dart';

class CustomAppbar extends StatelessWidget implements PreferredSizeWidget {
  final double _height = 56;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final AnimationController controller;

  const CustomAppbar(
      {Key? key, required this.scaffoldKey, required this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      preferredSize: Size.fromHeight(_height), // Set this height
      child: AppBar(
        title: const Text("Vytvořit události"),
        centerTitle: true,
        automaticallyImplyLeading: false,
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context),
        ),
        actions: <Widget>[
          IconButton(
            icon: AnimatedIcon(
              icon: AnimatedIcons.menu_close,
              progress: controller,
              semanticLabel: 'Show menu',
            ),
            onPressed: () {
              if (scaffoldKey.currentState?.isEndDrawerOpen == false) {
                scaffoldKey.currentState?.openEndDrawer();
                controller.forward();
              } else {
                scaffoldKey.currentState?.openDrawer();
                controller.reverse();
              }
            },
          ),
        ],
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(_height);
}
