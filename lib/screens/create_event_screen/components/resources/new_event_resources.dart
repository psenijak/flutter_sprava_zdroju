import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:vera/redux/events_state/events_actions.dart';
import 'package:vera/redux/store.dart';
import 'package:vera/screens/create_event_screen/components/resources/components/event_resource_list.dart';
import 'package:vera/screens/shared_components/autocomplete_input.dart';

class NewEventResources extends StatelessWidget {
  const NewEventResources({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (context, state) {
          return Column(
            children: [
              AutocompleteInput(
                hasTrailing: false,
                hintText: "Hledej zdroj:",
                suggestions: state.dialsState.resourceDialList!,
                onSelect: (item) => addResourceToNewEvent(item),
              ),
              EventResourceList(
                  eventResources: state.eventsState.newEvent!.resources),
            ],
          );
        });
  }
}
