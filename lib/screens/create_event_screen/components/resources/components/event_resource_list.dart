import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/models/resource_dial_model.dart';
import 'package:vera/redux/events_state/events_actions.dart';

import 'event_resource_item.dart';

class EventResourceList extends StatefulWidget {
  final List<ResourceDialModel> eventResources;
  const EventResourceList({Key? key, required this.eventResources})
      : super(key: key);

  @override
  _EventResourceListState createState() => _EventResourceListState();
}

class _EventResourceListState extends State<EventResourceList> {
  @override
  Widget build(BuildContext context) {
    final scrollController = ScrollController();

    return Padding(
      padding: const EdgeInsets.only(bottom: 10, top: 10),
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: widget.eventResources.length,
        controller: scrollController,
        padding: EdgeInsets.symmetric(horizontal: (5.w / 2)),
        itemBuilder: (BuildContext context, int i) {
          return EventResourceItem(
              resourceDialModel: widget.eventResources[i],
              onDelete: (value) => removeResourceToNewEvent(value));
        },
      ),
    );
  }
}
