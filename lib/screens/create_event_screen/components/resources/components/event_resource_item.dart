import 'package:flutter/material.dart';
import 'package:vera/models/resource_dial_model.dart';
import 'package:vera/screens/shared_components/container_wrapper.dart';

class EventResourceItem extends StatelessWidget {
  final ResourceDialModel resourceDialModel;
  final Function onDelete;
  const EventResourceItem(
      {Key? key, required this.resourceDialModel, required this.onDelete})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ContainerWrapper(
      outerPadding: const EdgeInsets.only(bottom: 10),
      child: ListTile(
        title: Text(resourceDialModel.name),
        trailing: TextButton(
          child: const Icon(Icons.delete_outline),
          onPressed: () {
            onDelete(resourceDialModel);
          },
          style: TextButton.styleFrom(primary: Colors.red),
        ),
      ),
    );
  }
}
