import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:vera/redux/events_state/events_actions.dart';
import 'package:vera/redux/store.dart';
import 'package:vera/screens/shared_components/autocomplete_input.dart';
import 'package:vera/screens/create_event_screen/components/participants/components/event_participant_list.dart';

class NewEventParticipants extends StatefulWidget {
  const NewEventParticipants({Key? key}) : super(key: key);

  @override
  _NewEventParticipantsState createState() => _NewEventParticipantsState();
}

class _NewEventParticipantsState extends State<NewEventParticipants> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, AppState>(
        converter: (store) => store.state,
        builder: (context, state) {
          return Column(
            children: [
              AutocompleteInput(
                hasTrailing: true,
                hintText: "Hledej osobu:",
                suggestions: state.dialsState.userDialList!,
                onSelect: (item) => addParticipantToNewEvent(item),
              ),
              EventParticipantList(
                  eventParticipants: state.eventsState.newEvent!.participants)
            ],
          );
        });
  }
}
