import 'package:flutter/material.dart';
import 'package:vera/models/user_dial_model.dart';
import 'package:vera/screens/shared_components/container_wrapper.dart';

class EventParticipantItem extends StatelessWidget {
  final UserDialModel userDial;
  final Function onDelete;
  const EventParticipantItem(
      {Key? key, required this.userDial, required this.onDelete})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ContainerWrapper(
      outerPadding: const EdgeInsets.only(bottom: 10),
      child: ListTile(
        title: Text(userDial.name),
        subtitle: Text(userDial.shortcut),
        trailing: TextButton(
          child: const Icon(Icons.delete_outline),
          onPressed: () {
            onDelete(userDial);
          },
          style: TextButton.styleFrom(primary: Colors.red),
        ),
      ),
    );
  }
}
