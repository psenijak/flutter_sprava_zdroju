import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/models/user_dial_model.dart';
import 'package:vera/redux/events_state/events_actions.dart';
import 'package:vera/screens/create_event_screen/components/participants/components/event_participant_item.dart';

class EventParticipantList extends StatefulWidget {
  final List<UserDialModel> eventParticipants;
  const EventParticipantList({Key? key, required this.eventParticipants})
      : super(key: key);

  @override
  _EventParticipantListState createState() => _EventParticipantListState();
}

class _EventParticipantListState extends State<EventParticipantList> {
  @override
  Widget build(BuildContext context) {
    final scrollController = ScrollController();

    return Padding(
      padding: const EdgeInsets.only(bottom: 10, top: 10),
      child: ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: widget.eventParticipants.length,
        controller: scrollController,
        padding: EdgeInsets.symmetric(horizontal: (5.w / 2)),
        itemBuilder: (BuildContext context, int i) {
          return EventParticipantItem(
              userDial: widget.eventParticipants[i],
              onDelete: (value) => removeParticipantToNewEvent(value));
        },
      ),
    );
  }
}
