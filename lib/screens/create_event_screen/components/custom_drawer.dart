import 'package:flutter/material.dart';

class CustomDrawer extends StatelessWidget {
  final Function onChange;
  const CustomDrawer({Key? key, required this.onChange}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Padding(
        padding: const EdgeInsets.only(top: 20.0, bottom: 10),
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListTile(
                title: Text(
                  'Událost',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Theme.of(context).primaryColor),
                ),
                onTap: () {
                  onChange(1);
                },
              ),
              ListTile(
                title: Text(
                  'Účastníci',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Theme.of(context).primaryColor),
                ),
                onTap: () {
                  onChange(2);
                },
              ),
              ListTile(
                title: Text(
                  'Zdroje',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Theme.of(context).primaryColor),
                ),
                onTap: () {
                  onChange(3);
                },
              ),
              const Spacer(),
              ListTile(
                title: ElevatedButton(
                  child: const Text("Odeslat"),
                  onPressed: () {},
                ),
              ),
              ListTile(
                  title: TextButton(
                      onPressed: () {},
                      child: const Text("Vynulovat"),
                      style: TextButton.styleFrom(
                          primary: Theme.of(context).primaryColor,
                          backgroundColor: Theme.of(context)
                              .primaryColor
                              .withOpacity(0.2)))),
            ],
          ),
        ),
      ),
    );
  }
}
