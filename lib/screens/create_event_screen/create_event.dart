import 'package:flutter/material.dart';
import 'package:vera/screens/create_event_screen/components/custom_appbar.dart';
import 'package:vera/screens/create_event_screen/components/custom_drawer.dart';
import 'package:vera/screens/create_event_screen/components/descriptions/new_event_descriptions.dart';
import 'package:vera/screens/create_event_screen/components/participants/new_event_participants.dart';
import 'package:vera/screens/create_event_screen/components/resources/new_event_resources.dart';

class CreateEvent extends StatefulWidget {
  const CreateEvent({Key? key}) : super(key: key);

  @override
  _CreateEventState createState() => _CreateEventState();
}

class _CreateEventState extends State<CreateEvent>
    with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  late AnimationController controller;
  Widget _body = const NewEventDescriptions();

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
      reverseDuration: const Duration(milliseconds: 300),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: CustomAppbar(controller: controller, scaffoldKey: _scaffoldKey),
      body: Scaffold(
        key: _scaffoldKey,
        endDrawer: CustomDrawer(onChange: setBody),
        onEndDrawerChanged: (isOpened) {
          if (isOpened) {
            controller.forward();
          } else {
            controller.reverse();
          }
        },
        body: _body,
      ),
    );
  }

  void setBody(int section) {
    Navigator.of(context).pop();
    switch (section) {
      case 1:
        setState(() {
          _body = const NewEventDescriptions();
        });
        break;
      case 2:
        setState(() {
          _body = const NewEventParticipants();
        });
        break;
      case 3:
        setState(() {
          _body = const NewEventResources();
        });
        break;
    }
  }
}
