import 'package:flutter/material.dart';
import 'package:vera/models/event_detail_model.dart';
import 'package:vera/screens/event_detail_screen/components/event_detail_tabs.dart';
import 'package:vera/screens/event_detail_screen/components/event_detail_toolbar.dart';
import 'package:vera/screens/event_detail_screen/components/event_info.dart';

class EventDetail extends StatefulWidget {
  const EventDetail({Key? key}) : super(key: key);

  @override
  _EventDetailState createState() => _EventDetailState();
}

class _EventDetailState extends State<EventDetail> {
  final EventDetailModel eventDetail = EventDetailModel(
      id: 1,
      title: "Setkání zaměstnanců",
      place: "Chlumec nad Cidlinou",
      dateSince: "08.04.2022 11:30",
      timeSince: "11:30",
      dateTo: "09.04.2022 23:30",
      timeTo: "12:30",
      isOwner: true,
      eventType: "Závazná",
      projectCode: "VERA-80-00100",
      projectName: "Projektové jméno",
      activity: "Návštěva lékaře",
      hostShortcut: "PSE",
      isWholeDay: false,
      hostName: "Pšenička Jiří, Ing",
      notification: true,
      confirm: true,
      trip: false,
      note: "note",
      attachmentUrl: "attachmentUrl",
      participants: [],
      resources: []);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: AppBar(
        title: const Text("Detail události"),
        centerTitle: true,
        backgroundColor: Theme.of(context).primaryColor,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            EventInfo(eventDetailModel: eventDetail),
            const EventDetailToolbar(),
            const EventDetailTabs(),
          ],
        ),
      ),
    );
  }
}
