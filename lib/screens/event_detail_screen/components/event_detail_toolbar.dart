import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/screens/reply_screen/reply.dart';
import 'package:vera/screens/shared_components/container_wrapper.dart';

class EventDetailToolbar extends StatelessWidget {
  const EventDetailToolbar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _scrollController = ScrollController();
    final List<Widget> _toolList = [
      _getItem(Icons.create, "Oprav", Theme.of(context).primaryColor, () {}),
      _getItem(Icons.delete_outline, "Ruš", Colors.red, () {}),
      _getItem(Icons.copy_outlined, "Kopíruj",
          Theme.of(context).primaryColorDark, () {}),
      _getItem(Icons.reply, "Odpovědět", const Color.fromRGBO(113, 48, 100, 1),
          () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (_) => const Reply()),
        );
      }),
      _getItem(Icons.attach_file_outlined, "Příloha",
          const Color.fromRGBO(79, 128, 115, 1), () {}),
    ];
    return ContainerWrapper(
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: _toolList.length,
        controller: _scrollController,
        // padding: EdgeInsets.symmetric(horizontal: (5.w / 2)),
        itemBuilder: (BuildContext context, int i) {
          return _toolList[i];
        },
      ),
      width: 95.w,
      height: 60,
      outerPadding: EdgeInsets.only(left: 5.w / 2, right: 5.w / 2, top: 10),
    );
    // return Padding(
    //   padding: EdgeInsets.only(left: 5.w / 2, right: 5.w / 2, top: 10),
    //   child: Container(
    //     decoration: BoxDecoration(
    //         boxShadow: [
    //           BoxShadow(
    //             color: Theme.of(context).cardColor,
    //             spreadRadius: 3,
    //             blurRadius: 7,
    //             offset: const Offset(1, 2), // changes position of shadow
    //           ),
    //         ],
    //         color: Theme.of(context).secondaryHeaderColor,
    //         borderRadius: const BorderRadius.all(Radius.circular(14))),
    //     width: 95.w,
    //     height: 60,
    //     child: ListView.builder(
    //       scrollDirection: Axis.horizontal,
    //       itemCount: _toolList.length,
    //       controller: _scrollController,
    //       // padding: EdgeInsets.symmetric(horizontal: (5.w / 2)),
    //       itemBuilder: (BuildContext context, int i) {
    //         return _toolList[i];
    //       },
    //     ),
    //   ),
    // );
  }

  Widget _getItem(IconData icon, String label, Color color, Function onPress) {
    return TextButton(
      style: TextButton.styleFrom(
        primary: color,
      ),
      onPressed: () {
        onPress();
        // Respond to button press
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            icon,
          ),
          Text(
            label,
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }
}
