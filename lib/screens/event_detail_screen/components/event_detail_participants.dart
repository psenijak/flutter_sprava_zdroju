import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class EventDetailParticipants extends StatefulWidget {
  const EventDetailParticipants({Key? key}) : super(key: key);

  @override
  _EventDetailParticipantsState createState() =>
      _EventDetailParticipantsState();
}

class _EventDetailParticipantsState extends State<EventDetailParticipants> {
  final scrollController = ScrollController();
  final List<String> participants = [
    "Jiří pšenička, Ing",
    "Jiří Matoušek, Ing",
    "Jan Stehno, Ing",
    "Jan Stehno, Ing",
    "Jan Stehno, Ing",
    "Jan Stehno, Ing",
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: participants.length,
      controller: scrollController,
      itemBuilder: (BuildContext context, int i) {
        return getListTile(participants[i]);
      },
    );
  }

  Widget getListTile(String name) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 6),
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Theme.of(context).cardColor,
                spreadRadius: 3,
                blurRadius: 7,
                offset: const Offset(1, 2), // changes position of shadow
              ),
            ],
            color: Theme.of(context).secondaryHeaderColor,
            borderRadius: const BorderRadius.all(Radius.circular(14))),
        width: 95.w,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(name,
                style: TextStyle(
                    color: Theme.of(context).primaryColorLight, fontSize: 18)),
            Text(
              "PSE",
              style: TextStyle(color: Theme.of(context).primaryColorDark),
            )
          ],
        ),
      ),
    );
  }
}
