import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/screens/event_detail_screen/components/event_detail_participants.dart';
import 'package:vera/screens/shared_components/container_wrapper.dart';

class EventDetailTabs extends StatefulWidget {
  const EventDetailTabs({Key? key}) : super(key: key);

  @override
  _EventDetailTabsState createState() => _EventDetailTabsState();
}

class _EventDetailTabsState extends State<EventDetailTabs>
    with TickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      initialIndex: 0,
      length: 2,
      vsync: this,
    );
  }

  @override
  Widget build(BuildContext context) {
    return ContainerWrapper(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            decoration: BoxDecoration(
                color: Theme.of(context).primaryColor,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(14),
                    topRight: Radius.circular(14))),
            child: TabBar(
              indicatorColor: Colors.white,
              labelColor: Colors.white,
              controller: _tabController,
              tabs: const [
                Tab(text: "Účastníci"),
                Tab(text: "Zdroje"),
              ],
            ),
          ),
          Expanded(
            child: TabBarView(controller: _tabController, children: const [
              EventDetailParticipants(),
              EventDetailParticipants(),
              // EventDetailResources(),
            ]),
          )
        ],
      ),
      outerPadding:
          EdgeInsets.only(left: 5.w / 2, right: 5.w / 2, top: 10, bottom: 10),
      height: 300,
      width: 95.w,
    );
  }
}
