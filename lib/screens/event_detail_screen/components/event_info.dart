import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/models/event_detail_model.dart';
import 'package:vera/screens/shared_components/container_wrapper.dart';

class EventInfo extends StatelessWidget {
  final EventDetailModel eventDetailModel;
  const EventInfo({required this.eventDetailModel, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _scrollController = ScrollController();
    final List<Widget> _checkboxes = [
      getCheckboxWithLabel("Notifikace:", true, context),
      getCheckboxWithLabel("Schválit:", true, context),
      getCheckboxWithLabel("Cesta:", false, context),
    ];

    return ContainerWrapper(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            eventDetailModel.title,
            style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontWeight: FontWeight.bold,
                fontSize: 18),
          ),
          _getRowWithIcon(Icons.place, eventDetailModel.place, context),
          _getRowWithIcon(
              Icons.access_time, eventDetailModel.dateSince, context),
          _getRowWithIcon(Icons.timelapse, eventDetailModel.dateTo, context),
          _getRowWithLabel("Událost:", eventDetailModel.eventType, context),
          _getRowWithLabel(
              "Kód projektu:", eventDetailModel.projectCode, context),
          _getRowWithLabel("Projektu:", eventDetailModel.projectName, context),
          _getRowWithLabel("Činnost:", eventDetailModel.activity, context),
          _getRowWithLabel(
              "Zkratka organizátora:", eventDetailModel.hostShortcut, context),
          _getRowWithLabel("Organizátor:", eventDetailModel.hostName, context),
          SizedBox(
            height: 30,
            child: ListView.builder(
              padding: const EdgeInsets.only(left: 3),
              scrollDirection: Axis.horizontal,
              itemCount: _checkboxes.length,
              controller: _scrollController,
              // padding: EdgeInsets.symmetric(horizontal: (5.w / 2)),
              itemBuilder: (BuildContext context, int i) {
                return _checkboxes[i];
              },
            ),
          ),
          _getRowWithLabel("Poznámka:", eventDetailModel.note, context),
        ],
      ),
      outerPadding: EdgeInsets.only(left: 5.w / 2, right: 5.w / 2, top: 10),
      width: 95.w,
      innerPadding: const EdgeInsets.all(10.0),
    );
  }

  Widget _getRowWithIcon(IconData icon, String text, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 4),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 4),
            child: Icon(
              icon,
              color: Theme.of(context).primaryColorDark,
            ),
          ),
          Flexible(
            child: Text(
              text,
              style: TextStyle(
                fontSize: 16,
                color: Theme.of(context).primaryColorDark,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _getRowWithLabel(String label, String text, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 4),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 4, left: 3),
            child: Text(
              label,
              style: TextStyle(
                fontSize: 16,
                color: Theme.of(context).primaryColorDark,
              ),
            ),
          ),
          Flexible(
            child: Text(
              text,
              style: TextStyle(
                fontSize: 16,
                color: Theme.of(context).primaryColorDark,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget getCheckboxWithLabel(
      String label, bool checked, BuildContext context) {
    return Row(children: [
      Text(
        label,
        style: TextStyle(
          fontSize: 16,
          color: Theme.of(context).primaryColorDark,
        ),
      ),
      SizedBox(
        height: 22,
        width: 22,
        child: Checkbox(
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          onChanged: (_) {},
          value: checked,
          activeColor: Theme.of(context).primaryColor,
        ),
      ),
      const SizedBox(
        width: 7,
      ),
    ]);
  }
}
