import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/redux/utils_profile_state/utils_profile_actions.dart';
import 'package:vera/screens/sign_in_screen/log_in.dart';

class LogoutButton extends StatelessWidget {
  const LogoutButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 2.h),
      child: SizedBox(
        width: 95.w,
        child: ElevatedButton(
          onPressed: () {
            logoutUser().then((value) => Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => const LogIn()),
                ));
          },
          child: const Text(
            'Odhlásit se',
            style: TextStyle(color: Colors.white),
          ),
          style: ElevatedButton.styleFrom(
            primary: Theme.of(context).primaryColor,
          ),
        ),
      ),
    );
  }
}
