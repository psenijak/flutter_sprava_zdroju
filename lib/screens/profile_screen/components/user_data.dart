import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/redux/store.dart';
import 'package:vera/redux/utils_profile_state/utils_profile_state.dart';
import 'package:vera/screens/shared_components/container_wrapper.dart';

class UserData extends StatefulWidget {
  const UserData({Key? key}) : super(key: key);

  @override
  _UserDataState createState() => _UserDataState();
}

class _UserDataState extends State<UserData> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, UtilsProfileState>(
        converter: (store) => store.state.utilsProfileState,
        builder: (context, state) {
          return ContainerWrapper(
            width: 95.w,
            outerPadding: EdgeInsets.only(left: 2.5.w, right: 2.5.w, top: 2.h),
            innerPadding: EdgeInsets.only(
                left: 2.5.w, right: 2.5.w, bottom: 2.h, top: 1.5.h),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Jméno:",
                  style: TextStyle(color: Theme.of(context).primaryColorDark),
                ),
                SizedBox(
                  height: 0.2.h,
                ),
                Text(
                  state.user!.user.name,
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 22),
                ),
                SizedBox(
                  height: 1.h,
                ),
                Text(
                  "Login:",
                  style: TextStyle(color: Theme.of(context).primaryColorDark),
                ),
                SizedBox(
                  height: 0.2.h,
                ),
                Text(
                  state.user!.user.login,
                  style: TextStyle(
                      color: Theme.of(context).primaryColorDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                SizedBox(
                  height: 1.h,
                ),
                Text(
                  "Doména:",
                  style: TextStyle(color: Theme.of(context).primaryColorDark),
                ),
                SizedBox(
                  height: 0.2.h,
                ),
                Text(
                  state.user!.user.domain,
                  style: TextStyle(
                      color: Theme.of(context).primaryColorDark,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                SizedBox(
                  height: 1.h,
                ),
                Text(
                  "Název funkčních zařazení:",
                  style: TextStyle(color: Theme.of(context).primaryColorDark),
                ),
                SizedBox(
                  height: 0.2.h,
                ),
                ...state.user!.user.positionList.map((e) => Text(
                      // state.user!.user.positionList[0].unitTitle,
                      e.unitTitle,
                      style: TextStyle(
                          color: Theme.of(context).primaryColorDark,
                          fontWeight: FontWeight.bold,
                          fontSize: 18),
                    ))
                // Text(
                //   // state.user!.user.positionList[0].unitTitle,
                //   state.user!.user.positionList.map((ut) => `ut.unitTitle`).toString(),
                //   style: TextStyle(
                //       color: Theme.of(context).primaryColorDark,
                //       fontWeight: FontWeight.bold,
                //       fontSize: 18),
                // ),
              ],
            ),
          );
        });
  }
}
