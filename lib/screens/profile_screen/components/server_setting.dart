import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/redux/store.dart';
import 'package:vera/redux/utils_profile_state/utils_profile_state.dart';
import 'package:vera/screens/add_server_screen/add_server.dart';
import 'package:vera/screens/profile_screen/profile.dart';
import 'package:vera/screens/shared_components/container_wrapper.dart';

class ServerSetting extends StatefulWidget {
  const ServerSetting({Key? key}) : super(key: key);

  @override
  _ServerSettingState createState() => _ServerSettingState();
}

class _ServerSettingState extends State<ServerSetting> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, UtilsProfileState>(
        converter: (store) => store.state.utilsProfileState,
        builder: (context, state) {
          return ContainerWrapper(
              width: 95.w,
              outerPadding:
                  EdgeInsets.only(left: 2.5.w, right: 2.5.w, top: 2.h),
              innerPadding: EdgeInsets.only(
                  left: 2.5.w, right: 2.5.w, bottom: 2.h, top: 1.5.h),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Aktuální server:",
                    style: TextStyle(color: Theme.of(context).primaryColorDark),
                  ),
                  SizedBox(
                    height: 0.2.h,
                  ),
                  Text(
                    state.server,
                    style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  SizedBox(
                    width: 95.w,
                    child: TextButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const AddServer(
                                    nextScreen: ProfileScreen())),
                          );
                        },
                        child: const Text("Změnit server"),
                        style: TextButton.styleFrom(
                            primary: Theme.of(context).primaryColor,
                            backgroundColor: Theme.of(context)
                                .primaryColor
                                .withOpacity(0.2))),
                  )
                ],
              ));
        });
  }
}
