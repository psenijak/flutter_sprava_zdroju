import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/redux/store.dart';
import 'package:vera/redux/utils_profile_state/utils_profile_actions.dart';
import 'package:vera/redux/utils_profile_state/utils_profile_state.dart';
import 'package:vera/screens/shared_components/container_wrapper.dart';
import 'package:vera/screens/shared_components/toggle_buton.dart';

class ThemeSwitch extends StatefulWidget {
  const ThemeSwitch({Key? key}) : super(key: key);

  @override
  _ThemeSwitchState createState() => _ThemeSwitchState();
}

class _ThemeSwitchState extends State<ThemeSwitch> {
  String getDefaultThemeMode(ThemeMode themeMode) {
    return themeMode.toString() == "ThemeMode.system"
        ? "Systém"
        : themeMode.toString() == "ThemeMode.dark"
            ? "Tmavý"
            : "Světlý";
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, UtilsProfileState>(
        converter: (store) => store.state.utilsProfileState,
        builder: (context, state) {
          return ContainerWrapper(
              outerPadding:
                  EdgeInsets.only(left: 2.5.w, right: 2.5.w, top: 2.h),
              innerPadding: EdgeInsets.only(
                  left: 2.5.w, right: 2.5.w, bottom: 2.h, top: 1.5.h),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Barevný motiv:",
                    style: TextStyle(color: Theme.of(context).primaryColorDark),
                  ),
                  ToggleButtonComponent(
                    maxSize: 10.w,
                    onSelect: updateTheme,
                    defaultValue:
                        getDefaultThemeMode(state.themeProvider.themeMode),
                    options: const ["Systém", "Tmavý", "Světlý"],
                  ),
                ],
              ));
        });
  }
}
