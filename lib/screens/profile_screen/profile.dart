import 'package:flutter/material.dart';
import 'package:vera/screens/home_screen/home.dart';
import 'package:vera/screens/profile_screen/components/logout_button.dart';
import 'package:vera/screens/profile_screen/components/server_setting.dart';
import 'package:vera/screens/profile_screen/components/theme_switch.dart';
import 'package:vera/screens/profile_screen/components/user_data.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Účet na nastavení"),
          centerTitle: true,
          backgroundColor: Theme.of(context).primaryColor,
          leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => const Home()),
                );
              }),
        ),
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: const [
              ThemeSwitch(),
              ServerSetting(),
              UserData(),
              LogoutButton()
            ],
          ),
        ));
  }
}
