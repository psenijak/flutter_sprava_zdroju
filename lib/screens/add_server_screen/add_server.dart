import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/redux/store.dart';
import 'package:vera/redux/utils_profile_state/utils_profile_actions.dart';
import 'package:vera/redux/utils_profile_state/utils_profile_state.dart';
import 'package:vera/screens/shared_components/input_field.dart';
import 'package:vera/screens/sign_in_screen/log_in.dart';
import 'package:vera/utils/error_snackbar_manager.dart';

class AddServer extends StatefulWidget {
  final Widget nextScreen;
  const AddServer({Key? key, required this.nextScreen}) : super(key: key);

  @override
  _AddServerState createState() => _AddServerState();
}

class _AddServerState extends State<AddServer> {
  final _addressController = TextEditingController();
  final FocusNode _addressFocusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, UtilsProfileState>(
        converter: (store) => store.state.utilsProfileState,
        builder: (context, state) {
          return Scaffold(
            body: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Align(
                    alignment: Alignment.center,
                    child: Image.asset('assets/images/vera_logo.png')),
                SizedBox(
                  height: 3.h,
                ),
                Center(
                  child: Text(
                    "Rezervace zdrojů",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Theme.of(context).primaryColorLight,
                        fontWeight: FontWeight.bold,
                        fontSize: 28),
                  ),
                ),
                SizedBox(
                  height: 10.h,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5.w / 2),
                  child: Text(
                    "Níže prosím vložte adresu vašeho serveru:",
                    style: TextStyle(color: Theme.of(context).primaryColorDark),
                  ),
                ),
                SizedBox(
                  height: 1.h,
                ),
                InputField(
                    initialValue: state.server,
                    onChanged: (value) {
                      setServer(value);
                    },
                    inputController: _addressController,
                    focusNode: _addressFocusNode,
                    label: "url serveru",
                    keyboardType: TextInputType.text,
                    hasMaxLine: true),
                SizedBox(
                  height: 1.h,
                ),
              ],
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                if (state.server.isEmpty) {
                  ErrorManager.showError("Adresa serveru musí být vyplněna!");
                } else {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => widget.nextScreen),
                  );
                }
              },
              backgroundColor: Theme.of(context).primaryColor,
              child: const Icon(
                Icons.arrow_forward,
                color: Colors.white,
              ),
            ),
          );
        });
  }
}
