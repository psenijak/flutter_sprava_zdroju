import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/screens/shared_components/appbar_item_container.dart';
import 'package:vera/screens/filter_event_list_screen/Filter_event_list.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final double _height = 120;
  const CustomAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      preferredSize: Size.fromHeight(_height), // Set this height
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SizedBox(
            width: 95.w,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                AppBarContainer(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8),
                      child: Icon(
                        Icons.arrow_back_ios,
                        size: 25,
                        color: Theme.of(context).primaryColorDark,
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                    }),
                SizedBox(
                  width: 5.w / 2,
                ),
                Expanded(
                  flex: 1,
                  child: AppBarContainer(
                      containerWidth: double.infinity,
                      child: Center(
                        child: Text(
                          "Hledej",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              color: Theme.of(context).primaryColorDark),
                        ),
                      ),
                      onTap: () {}),
                ),
                SizedBox(
                  width: 5.w / 2,
                ),
                AppBarContainer(
                    child: Icon(
                      Icons.filter_list,
                      size: 25,
                      color: Theme.of(context).primaryColorDark,
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const FilterEventList()),
                      );
                    }),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(_height);
}
