import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/screens/reply_screen/reply.dart';

class EventItemToolbar extends StatelessWidget {
  final int eventId;
  final bool isHost;
  const EventItemToolbar(
      {Key? key, required this.eventId, required this.isHost})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _scrollController = ScrollController();
    final List<Widget> _toolList = [
      _getItem(Icons.create, "Oprav", Theme.of(context).primaryColor, () {}),
      _getItem(Icons.delete_outline, "Ruš", Colors.red, () {}),
      _getItem(Icons.copy_outlined, "Kopíruj",
          Theme.of(context).primaryColorDark, () {}),
      _getItem(Icons.reply, "Odpovědět", const Color.fromRGBO(113, 48, 100, 1),
          () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (_) => const Reply()),
        );
      }),
      _getItem(Icons.format_align_left, "Detail",
          const Color.fromRGBO(79, 128, 115, 1), () {}),
    ];

    final List<Widget> _hostList = [..._toolList];
    final List<Widget> _participantList = [
      _toolList[2],
      _toolList[3],
      _toolList[4]
    ];

    return SizedBox(
      width: 95.w,
      height: 60,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: isHost ? _hostList.length : _participantList.length,
        controller: _scrollController,
        itemBuilder: (BuildContext context, int i) {
          return isHost ? _hostList[i] : _participantList[i];
        },
      ),
    );
  }

  Widget _getItem(IconData icon, String label, Color color, Function onPress) {
    return TextButton(
      style: TextButton.styleFrom(
        primary: color,
      ),
      onPressed: () {
        onPress();
        // Respond to button press
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
            icon,
          ),
          Text(
            label,
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }
}
