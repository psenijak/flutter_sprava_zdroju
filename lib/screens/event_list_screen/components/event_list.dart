import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/models/event_model.dart';
import 'package:vera/screens/event_list_screen/components/event_item.dart';

class EventList extends StatefulWidget {
  const EventList({Key? key}) : super(key: key);

  @override
  _EventListState createState() => _EventListState();
}

class _EventListState extends State<EventList> {
  final scrollController = ScrollController();

  final List<EventModel> _events = <EventModel>[
    EventModel(1, "Setkání zaměstnanců", "Chlumec nad Cidlinou",
        "08.04.2022 11:30", "09.04.2022 23:30", true),
    EventModel(2, "Setkání zaměstnanců po 25 letech", "Chlumec nad Cidlinou",
        "08.04.2047 11:30", "09.04.2047 23:30", false),
    EventModel(3, "Setkání zaměstnanců po 45 letech", "Chlumec nad Cidlinou",
        "08.04.2067 11:30", "09.04.2067 23:30", false),
    EventModel(4, "Setkání zaměstnanců po 45 letech", "Chlumec nad Cidlinou",
        "08.04.2067 11:30", "09.04.2067 23:30", true),
    EventModel(5, "Setkání zaměstnanců po 45 letech", "Chlumec nad Cidlinou",
        "08.04.2067 11:30", "09.04.2067 23:30", false),
    EventModel(6, "Setkání zaměstnanců po 45 letech", "Chlumec nad Cidlinou",
        "08.04.2067 11:30", "09.04.2067 23:30", true),
    EventModel(7, "Setkání zaměstnanců po 45 letech", "Chlumec nad Cidlinou",
        "08.04.2067 11:30", "09.04.2067 23:30", false),
  ];

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () {
        return Future.value(true);
      },
      child: Padding(
        padding: const EdgeInsets.only(bottom: 10),
        child: ListView.builder(
          itemCount: _events.length,
          controller: scrollController,
          padding: EdgeInsets.symmetric(horizontal: (5.w / 2)),
          itemBuilder: (BuildContext context, int i) {
            return EventItem(event: _events[i]);
          },
        ),
      ),
    );
  }
}
