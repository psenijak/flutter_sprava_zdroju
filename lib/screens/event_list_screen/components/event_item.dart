import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/models/event_model.dart';
import 'package:vera/screens/event_detail_screen/event_detail.dart';
import 'package:vera/screens/shared_components/container_wrapper.dart';

import 'event_item_toolbar.dart';

class EventItem extends StatelessWidget {
  final EventModel event;
  const EventItem({required this.event, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const EventDetail()),
            );
          },
          child: ContainerWrapper(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  event.title,
                  style: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
                _getRow(Icons.place, event.place, context, null),
                _getRow(Icons.access_time, event.since, context, null),
                _getRow(Icons.timelapse, event.to, context, null),
                _getRow(
                  Icons.person,
                  event.isOwner ? "Organizátor" : "Účastník",
                  context,
                  event.isOwner
                      // ? const Color.fromRGBO(0, 255, 127, 1)
                      // : const Color.fromRGBO(250, 128, 114, 1),
                      ? Colors.green
                      : Colors.red,
                ),
                EventItemToolbar(
                  eventId: event.id,
                  isHost: event.isOwner,
                ),
              ],
            ),
            innerPadding: const EdgeInsets.all(10.0),
            width: 95.w,
          )),
    );
  }

  Widget _getRow(
      IconData icon, String text, BuildContext context, Color? color) {
    return Padding(
      padding: const EdgeInsets.only(top: 4),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 4),
            child: Icon(
              icon,
              color: color ?? Theme.of(context).primaryColorDark,
            ),
          ),
          Flexible(
            child: Text(
              text,
              style: TextStyle(
                fontSize: 16,
                color: color ?? Theme.of(context).primaryColorDark,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
