import 'package:flutter/material.dart';
import 'package:vera/models/operation_model.dart';

class BoxItem extends StatelessWidget {
  final OperationModel operation;
  const BoxItem({Key? key, required this.operation}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => operation.onClickPush),
        );
      },
      child: Ink(
        padding: const EdgeInsets.only(top: 30, bottom: 30),
        decoration: BoxDecoration(
            boxShadow: MediaQuery.of(context).platformBrightness !=
                    Brightness.dark
                ? [
                    BoxShadow(
                      color: Theme.of(context).cardColor.withOpacity(0.5),
                      spreadRadius: 3,
                      blurRadius: 7,
                      offset: const Offset(1, 2), // changes position of shadow
                    ),
                  ]
                : [],
            color: Theme.of(context).cardColor.withOpacity(0.5),
            borderRadius: const BorderRadius.all(Radius.circular(20))),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              operation.icon,
              color: Theme.of(context).primaryColorLight,
              size: 35,
            ),
            Text(
              operation.name,
              style: TextStyle(
                  color: Theme.of(context).primaryColorLight,
                  fontWeight: FontWeight.bold),
            )
          ],
        ),
      ),
    );
  }
}
