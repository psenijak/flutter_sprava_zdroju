import 'package:flutter/material.dart';
import 'package:vera/models/operation_model.dart';
import 'package:vera/screens/create_event_screen/create_event.dart';
import 'package:vera/screens/event_list_screen/events.dart';
import 'package:vera/screens/profile_screen/profile.dart';

import 'box_item.dart';

class Boxes extends StatefulWidget {
  const Boxes({Key? key}) : super(key: key);

  @override
  _BoxesState createState() => _BoxesState();
}

class _BoxesState extends State<Boxes> {
  final List<OperationModel> _operations = <OperationModel>[
    OperationModel("Události", Icons.list, const Events()),
    OperationModel("Kalendář", Icons.calendar_today_rounded, const Events()),
    OperationModel("Schvalování událostí", Icons.check, const Events()),
    OperationModel("Nová událost", Icons.add, const CreateEvent()),
    OperationModel("Účet a nastavení", Icons.person, const ProfileScreen()),
  ];

  @override
  Widget build(BuildContext context) {
    return GridView.count(
        mainAxisSpacing: 15,
        crossAxisSpacing: 15,
        padding: const EdgeInsets.all(15),
        crossAxisCount: 2,
        children: _operations
            .map((operation) => BoxItem(operation: operation))
            .toList());
  }
}
