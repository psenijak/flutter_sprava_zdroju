import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/screens/add_server_screen/add_server.dart';
import 'package:vera/screens/sign_in_screen/log_in.dart';

class ChangeServerButton extends StatefulWidget {
  const ChangeServerButton({Key? key}) : super(key: key);

  @override
  _ChangeServerButtonState createState() => _ChangeServerButtonState();
}

class _ChangeServerButtonState extends State<ChangeServerButton> {
  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => const AddServer(nextScreen: LogIn())),
        );
      },
      child: const Text("Změnit server"),
      style: OutlinedButton.styleFrom(
          side: const BorderSide(
            color: Colors.transparent,
            style: BorderStyle.solid,
          ),
          minimumSize: Size(95.w, 40),
          primary: Theme.of(context).primaryColor),
    );
  }
}
