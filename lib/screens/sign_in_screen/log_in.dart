import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/redux/store.dart';
import 'package:vera/redux/utils_profile_state/utils_profile_actions.dart';
import 'package:vera/redux/utils_profile_state/utils_profile_state.dart';
import 'package:vera/screens/home_screen/home.dart';
import 'package:vera/screens/shared_components/input_field.dart';

import 'components/change_server_button.dart';

class LogIn extends StatefulWidget {
  const LogIn({Key? key}) : super(key: key);

  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  final _nameController = TextEditingController();
  final _passwordController = TextEditingController();
  final _domainController = TextEditingController();
  final FocusNode _nameFocusNode = FocusNode();
  final FocusNode _passwordFocusNode = FocusNode();
  final FocusNode _domainFocusNode = FocusNode();
  String username = "tajemnik";
  String password = "tajemnik";
  String domain = "";

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, UtilsProfileState>(
        converter: (store) => store.state.utilsProfileState,
        builder: (context, state) {
          return Scaffold(
            resizeToAvoidBottomInset: false,
            body: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height:
                      MediaQuery.of(context).size.height > 600 ? 20.h : 13.h,
                ),
                Align(
                    alignment: Alignment.center,
                    child: Image.asset('assets/images/vera_logo.png')),
                SizedBox(
                  height: 2.h,
                ),
                Text(
                  "Rezervace zdrojů",
                  style: TextStyle(
                      color: Theme.of(context).primaryColorLight,
                      fontWeight: FontWeight.bold,
                      fontSize: 28),
                ),
                SizedBox(
                  height: 15.h,
                ),
                InputField(
                    initialValue: username,
                    onChanged: (value) {
                      setState(() {
                        username = value;
                      });
                    },
                    inputController: _nameController,
                    focusNode: _nameFocusNode,
                    label: "email",
                    keyboardType: TextInputType.text,
                    hasMaxLine: true),
                SizedBox(
                  height: 1.h,
                ),
                InputField(
                  initialValue: password,
                  onChanged: (value) {
                    setState(() {
                      password = value;
                    });
                  },
                  inputController: _passwordController,
                  focusNode: _passwordFocusNode,
                  label: "heslo",
                  keyboardType: TextInputType.text,
                  hasMaxLine: true,
                ),
                SizedBox(
                  height: 1.h,
                ),
                InputField(
                  initialValue: domain,
                  onChanged: (value) {
                    setState(() {
                      domain = value;
                    });
                  },
                  inputController: _domainController,
                  focusNode: _domainFocusNode,
                  label: "doména",
                  keyboardType: TextInputType.text,
                  hasMaxLine: true,
                ),
                SizedBox(
                  height: 3.h,
                ),
                ElevatedButton(
                  onPressed: () {
                    loginUser(username, password, domain)
                        .then((value) => value == true
                            ? Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const Home()),
                              )
                            : null);
                  },
                  child: state.isLoading
                      ? Container(
                          height: 20,
                          width: 20,
                          margin: const EdgeInsets.all(5),
                          child: const CircularProgressIndicator(
                            strokeWidth: 2.0,
                            valueColor: AlwaysStoppedAnimation(Colors.white),
                          ),
                        )
                      : const Text(
                          "Přihlásit se",
                          style: TextStyle(color: Colors.white),
                        ),
                  style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)), // <-- Radius

                    minimumSize: Size(95.w, 40),
                    primary: Theme.of(context)
                        .primaryColor, // background/ foreground
                  ),
                ),
                Spacer(),
                const SafeArea(child: ChangeServerButton()),
                SizedBox(
                  height: 2.h,
                ),
              ],
            ),
          );
        });
  }
}
