import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import 'dropdown_item.dart';

class DropDownItem extends StatefulWidget {
  const DropDownItem({Key? key}) : super(key: key);

  @override
  _DropDownItemState createState() => _DropDownItemState();
}

class _DropDownItemState extends State<DropDownItem> {
  final List<DropdownMenuItem<String>> _orderBy = [
    const DropdownMenuItem(child: Text("Vzestupně"), value: "ASC"),
    const DropdownMenuItem(child: Text("Sestupně"), value: "DESC"),
  ];
  final List<DropdownMenuItem<String>> _sortBy = [
    const DropdownMenuItem(child: Text("Název události"), value: "title"),
    const DropdownMenuItem(child: Text("Místo"), value: "place"),
    const DropdownMenuItem(child: Text("Čas začátku"), value: "since"),
    const DropdownMenuItem(child: Text("Čas konce"), value: "to"),
  ];

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(
          height: 15.h,
        ),
        DropdownItem(list: _sortBy, onChanged: () {}, defaultValue: "title"),
        DropdownItem(list: _orderBy, onChanged: () {}, defaultValue: "ASC"),
      ],
    );
  }
}
