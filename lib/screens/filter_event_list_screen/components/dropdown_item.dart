import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/screens/shared_components/container_wrapper.dart';

class DropdownItem extends StatefulWidget {
  final List<DropdownMenuItem<dynamic>> list;
  final VoidCallback onChanged;
  final dynamic defaultValue;
  const DropdownItem(
      {Key? key,
      required this.list,
      required this.onChanged,
      required this.defaultValue})
      : super(key: key);

  @override
  __SettingCardWidgetState createState() => __SettingCardWidgetState();
}

class __SettingCardWidgetState extends State<DropdownItem> {
  @override
  Widget build(BuildContext context) {
    return ContainerWrapper(
        child: DropdownButtonHideUnderline(
          child: DropdownButton<dynamic>(
            items: widget.list,
            value: widget.defaultValue,
            onChanged: (value) {
              //TODO - dodelat zmenu stavu
              widget.onChanged();
            },
            elevation: 8,
            style: TextStyle(
                color: Theme.of(context).primaryColorDark, fontSize: 16),
            icon: Icon(
              Icons.keyboard_arrow_down,
              color: Theme.of(context).primaryColorDark,
            ),
          ),
        ),
        width: 95.w,
        outerPadding: EdgeInsets.only(top: 30, left: 5.w / 2, right: 5.w / 2),
        innerPadding: const EdgeInsets.all(10));
  }
}
