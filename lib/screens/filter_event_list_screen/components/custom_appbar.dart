import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:vera/screens/shared_components/appbar_item_container.dart';

class CustomAppbar extends StatelessWidget implements PreferredSizeWidget {
  final double _height = 76;
  const CustomAppbar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      preferredSize: Size.fromHeight(_height), // Set this height
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: SizedBox(
            width: 95.w,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                AppBarContainer(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8),
                      child: Icon(
                        Icons.arrow_back_ios,
                        size: 25,
                        color: Theme.of(context).primaryColorDark,
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                    }),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(_height);
}
