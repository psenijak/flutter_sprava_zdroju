import 'package:flutter/material.dart';

import 'components/custom_appbar.dart';
import 'components/dropdowns.dart';

class FilterEventList extends StatefulWidget {
  const FilterEventList({Key? key}) : super(key: key);

  @override
  _FilterEventListState createState() => _FilterEventListState();
}

class _FilterEventListState extends State<FilterEventList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      appBar: const CustomAppbar(),
      body: const DropDownItem(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        backgroundColor: Theme.of(context).primaryColor,
        child: const Icon(
          Icons.save,
          color: Colors.white,
        ),
      ),
    );
  }
}
