import 'dart:convert';

class UserPositionModel {
  int id;
  String title;
  String unitTitle;
  String unitShortcut;
  bool defaultPos;
  String shortcut;

  UserPositionModel(
      {required this.id,
      required this.title,
      required this.unitTitle,
      required this.unitShortcut,
      required this.defaultPos,
      required this.shortcut});

  factory UserPositionModel.fromJson(Map parsedJson) {
    return UserPositionModel(
      id: parsedJson['id'] ?? 0,
      title: parsedJson['nazev'] ?? "",
      unitTitle: parsedJson['utvarNazev'] ?? "",
      unitShortcut: parsedJson['utvarZkratka'] ?? "",
      defaultPos: parsedJson['vychozi'] ?? false,
      shortcut: parsedJson['zkratka'] ?? "",
    );
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "nazev": title,
      "utvarNazev": unitTitle,
      "utvarZkratka": unitShortcut,
      "vychozi": defaultPos,
      "zkratka": shortcut
    };
  }

  static List<UserPositionModel> listFromJson(List<dynamic> json) {
    return json.map((value) => UserPositionModel.fromJson(value)).toList();
  }

  static List<dynamic> listToJson(List<UserPositionModel> json) {
    return json.map((value) => value.toJson()).toList();
  }
}
