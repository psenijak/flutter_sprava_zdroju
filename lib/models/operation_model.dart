import 'package:flutter/material.dart';

class OperationModel {
  String name;
  IconData icon;
  Widget onClickPush;

  OperationModel(this.name, this.icon, this.onClickPush);
}
