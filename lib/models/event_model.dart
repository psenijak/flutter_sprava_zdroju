class EventModel {
  int id;
  String title;
  String place;
  String since;
  String to;
  bool isOwner;

  EventModel(
      this.id, this.title, this.place, this.since, this.to, this.isOwner);
}
