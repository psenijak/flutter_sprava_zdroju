import 'package:intl/intl.dart';
import 'package:vera/utils/date_time_helper.dart';

class ReplyModel {
  String response;
  String responseDate;
  bool createTrip;
  String vehicle;
  String shortcut;
  String name;
  String dateSince;
  String timeSince;
  String dateTo;
  String timeTo;
  String note;
  String activity;
  bool isRequired;
  String needConfirm;

  ReplyModel(
      {required this.response,
      required this.responseDate,
      required this.createTrip,
      required this.vehicle,
      required this.shortcut,
      required this.name,
      required this.dateSince,
      required this.timeSince,
      required this.dateTo,
      required this.timeTo,
      required this.note,
      required this.activity,
      required this.isRequired,
      required this.needConfirm});

  factory ReplyModel.initial() {
    return ReplyModel(
        response: 'Přijmout',
        responseDate: DateFormat('dd-MM-yyyy').format(DateTime.now()),
        createTrip: false,
        vehicle: '',
        shortcut: 'PSE',
        name: 'Jiří Pšenička, Ing',
        dateSince: DateFormat('dd-MM-yyyy').format(DateTime.now()),
        timeSince: DateFormat.Hm().format(DateTime.now().toLocal().roundDown()),
        dateTo: DateFormat('dd-MM-yyyy')
            .format(DateTime.now().toLocal().add(const Duration(days: 1))),
        timeTo: DateFormat.Hm()
            .format(DateTime.now().roundDown().add(const Duration(hours: 1))),
        note: '',
        activity: '',
        isRequired: true,
        needConfirm: '');
  }

  ReplyModel copyWith({
    String? response,
    String? responseDate,
    bool? createTrip,
    String? vehicle,
    String? shortcut,
    String? name,
    String? dateSince,
    String? timeSince,
    String? dateTo,
    String? timeTo,
    String? note,
    String? activity,
    bool? isRequired,
    String? needConfirm,
  }) {
    return ReplyModel(
        response: response ?? this.response,
        responseDate: responseDate ?? this.responseDate,
        createTrip: createTrip ?? this.createTrip,
        vehicle: vehicle ?? this.vehicle,
        shortcut: shortcut ?? this.shortcut,
        name: name ?? this.name,
        dateSince: dateSince ?? this.dateSince,
        timeSince: timeSince ?? this.timeSince,
        dateTo: dateTo ?? this.dateTo,
        timeTo: timeTo ?? this.timeTo,
        note: note ?? this.note,
        activity: activity ?? this.activity,
        isRequired: isRequired ?? this.isRequired,
        needConfirm: needConfirm ?? this.needConfirm);
  }
}
