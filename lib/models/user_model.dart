import 'dart:convert';

import 'package:vera/models/user_position_model.dart';

class UserModel {
  int id;
  String name;
  String domain;
  String login;
  List<UserPositionModel> positionList = [];

  UserModel(
      {required this.id,
      required this.name,
      required this.domain,
      required this.login,
      required this.positionList});

  factory UserModel.fromJson(Map parsedJson) {
    return UserModel(
        id: parsedJson['id'] ?? 0,
        name: parsedJson['celeJmeno'] ?? "",
        domain: parsedJson['domain'] ?? "",
        login: parsedJson['login'] ?? "",
        positionList:
            UserPositionModel.listFromJson(parsedJson["funkcniZarazeni"]));
  }

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "celeJmeno": name,
      "domain": domain,
      "login": login,
      "funkcniZarazeni": UserPositionModel.listToJson(positionList)
    };
  }
}
