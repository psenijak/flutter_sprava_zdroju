import 'dart:convert';

import 'package:vera/models/user_model.dart';

class UserLoginModel {
  String authToken;
  String tokenType;
  UserModel user;

  UserLoginModel(
      {required this.authToken, required this.tokenType, required this.user});

  factory UserLoginModel.fromJson(Map parsedJson) {
    return UserLoginModel(
        authToken: parsedJson['authToken'] ?? "",
        tokenType: parsedJson['tokenType'] ?? "",
        user: UserModel.fromJson(parsedJson["user"]));
  }

  Map<String, dynamic> toJson() {
    return {
      "authToken": authToken,
      "tokenType": tokenType,
      "user": user.toJson(),
    };
  }
}
