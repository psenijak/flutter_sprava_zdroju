class ParticipantModel {
  String name;
  String shortcut;
  bool confirmed;

  ParticipantModel(this.name, this.shortcut, this.confirmed);
}
