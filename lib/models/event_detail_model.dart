import 'package:vera/models/resource_dial_model.dart';
import 'package:vera/models/user_dial_model.dart';
import 'package:intl/intl.dart';
import 'package:vera/utils/date_time_helper.dart';

class EventDetailModel {
  int id;
  String title;
  String place;
  String dateSince;
  String timeSince;
  String dateTo;
  String timeTo;
  bool isWholeDay;
  bool isOwner;
  String eventType;
  String projectCode;
  String projectName;
  String activity;
  String hostShortcut;
  String hostName;
  bool notification;
  bool confirm;
  bool trip;
  String note;
  String attachmentUrl;
  List<UserDialModel> participants;
  List<ResourceDialModel> resources;

  EventDetailModel(
      {required this.id,
      required this.title,
      required this.place,
      required this.dateSince,
      required this.timeSince,
      required this.dateTo,
      required this.timeTo,
      required this.isOwner,
      required this.eventType,
      required this.projectCode,
      required this.projectName,
      required this.isWholeDay,
      required this.activity,
      required this.hostShortcut,
      required this.hostName,
      required this.notification,
      required this.confirm,
      required this.trip,
      required this.note,
      required this.attachmentUrl,
      required this.participants,
      required this.resources});

  factory EventDetailModel.initial() {
    return EventDetailModel(
        id: 1,
        title: '',
        place: '',
        dateSince: DateFormat('dd-MM-yyyy').format(DateTime.now()),
        timeSince: DateFormat.Hm().format(DateTime.now().toLocal().roundDown()),
        dateTo: DateFormat('dd-MM-yyyy')
            .format(DateTime.now().toLocal().add(const Duration(days: 1))),
        timeTo: DateFormat.Hm()
            .format(DateTime.now().roundDown().add(const Duration(hours: 1))),
        isOwner: true,
        eventType: 'Předběžná',
        projectCode: '',
        projectName: '',
        isWholeDay: false,
        activity: '',
        hostShortcut: '',
        hostName: '',
        notification: true,
        confirm: true,
        trip: false,
        note: '',
        attachmentUrl: '',
        participants: [],
        resources: []);
  }

  EventDetailModel copyWith({
    int? id,
    String? title,
    String? place,
    String? dateSince,
    String? timeSince,
    String? dateTo,
    String? timeTo,
    bool? isOwner,
    String? eventType,
    String? projectCode,
    String? projectName,
    String? activity,
    String? hostShortcut,
    String? hostName,
    bool? notification,
    bool? confirm,
    bool? trip,
    bool? isWholeDay,
    String? note,
    String? attachmentUrl,
    List<UserDialModel>? participants,
    List<ResourceDialModel>? resources,
  }) {
    return EventDetailModel(
        id: id ?? this.id,
        title: title ?? this.title,
        place: place ?? this.place,
        dateSince: dateSince ?? this.dateSince,
        timeSince: timeSince ?? this.timeSince,
        dateTo: dateTo ?? this.dateTo,
        timeTo: timeTo ?? this.timeTo,
        isOwner: isOwner ?? this.isOwner,
        eventType: eventType ?? this.eventType,
        projectCode: projectCode ?? this.projectCode,
        projectName: projectName ?? this.projectName,
        isWholeDay: isWholeDay ?? this.isWholeDay,
        activity: activity ?? this.activity,
        hostShortcut: hostShortcut ?? this.hostShortcut,
        hostName: hostName ?? this.hostName,
        notification: notification ?? this.notification,
        confirm: confirm ?? this.confirm,
        trip: trip ?? this.trip,
        note: note ?? this.note,
        attachmentUrl: attachmentUrl ?? this.attachmentUrl,
        participants: participants ?? this.participants,
        resources: resources ?? this.resources);
  }
}
