import 'package:vera/redux/dials_state/dials_actions.dart';
import 'package:vera/redux/dials_state/dials_state.dart';

dialsReducer(DialsState prevState, dynamic action) {
  if (action is SetDialStateAction) {
    final payload = action.dialsState;
    return prevState.copyWith(
        userDialList: payload.userDialList ?? prevState.userDialList,
        resourceDialList:
            payload.resourceDialList ?? prevState.resourceDialList);
  }
  return prevState;
}
