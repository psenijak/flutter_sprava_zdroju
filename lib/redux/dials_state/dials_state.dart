import 'package:vera/models/resource_dial_model.dart';
import 'package:vera/models/user_dial_model.dart';

class DialsState {
  final List<UserDialModel>? userDialList;
  final List<ResourceDialModel>? resourceDialList;

  DialsState({required this.userDialList, required this.resourceDialList});

  factory DialsState.initial() {
    return DialsState(userDialList: [
      UserDialModel(1, "Jiří Pšenička, Ing", "PSE"),
      UserDialModel(2, "Jakub Pšenička", "PSE2"),
      UserDialModel(3, "Jakub Pšenička2", "PSE3"),
      UserDialModel(4, "Jakub Pšenička3", "PSE4"),
    ], resourceDialList: [
      ResourceDialModel(1, "Projektor"),
      ResourceDialModel(2, "Zasedačka"),
      ResourceDialModel(3, "Notebook"),
      ResourceDialModel(4, "Kávovar"),
    ]);
  }

  DialsState copyWith(
      {List<UserDialModel>? userDialList,
      List<ResourceDialModel>? resourceDialList}) {
    return DialsState(
        userDialList: userDialList, resourceDialList: resourceDialList);
  }
}
