import 'package:meta/meta.dart';
import 'package:vera/redux/dials_state/dials_state.dart';

@immutable
class SetDialStateAction {
  final DialsState dialsState;

  const SetDialStateAction(this.dialsState);
}

Future<void> loadDials() async {
  //
  // Redux.store!.dispatch(
  //     SetDialStateAction(DialsState(userDialList: userDialList));
  //
}
