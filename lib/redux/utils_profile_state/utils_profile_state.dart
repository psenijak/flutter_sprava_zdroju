import 'package:flutter/material.dart';
import 'package:vera/models/user_login_model.dart';
import 'package:vera/models/user_model.dart';
import 'package:vera/utils/themes.dart';

class UtilsProfileState {
  final ThemeProvider themeProvider;
  final String server;
  final UserLoginModel? user;
  final bool isLoading;

  UtilsProfileState(
      {required this.themeProvider,
      required this.server,
      required this.user,
      required this.isLoading});

  factory UtilsProfileState.initial() {
    return UtilsProfileState(
        themeProvider: ThemeProvider(ThemeMode.system),
        server: '',
        user: null,
        isLoading: false);
  }

  UtilsProfileState copyWith(
      {ThemeProvider? themeProvider,
      String? server,
      UserLoginModel? user,
      bool? isLoading}) {
    return UtilsProfileState(
        themeProvider: themeProvider ?? this.themeProvider,
        server: server ?? this.server,
        user: user ?? this.user,
        isLoading: isLoading ?? this.isLoading);
  }
}
