import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:vera/models/user_login_model.dart';
import 'package:vera/redux/utils_profile_state/utils_profile_state.dart';
import 'package:vera/utils/error_snackbar_manager.dart';
import 'package:vera/utils/local_storage_manager.dart';
import 'package:vera/utils/themes.dart';
import 'package:http/http.dart' as http;

import '../store.dart';

Map<String, String> requestHeaders = {
  'Content-type': 'application/x-www-form-urlencoded',
  'Accept': 'application/json',
  'X-Vera-API-Version': '1.6.0',
  'X-Vera-ClientIPAddr': '1'
};

@immutable
class SetUtilsProfileAction {
  final UtilsProfileState utilsProfileState;

  const SetUtilsProfileAction(this.utilsProfileState);
}

void updateTheme(String newTheme) {
  ThemeMode theme = newTheme == 'Systém'
      ? ThemeMode.system
      : newTheme == 'Tmavý'
          ? ThemeMode.dark
          : ThemeMode.light;
  StorageManager.storeTheme(theme);
  Redux.store!.dispatch(SetUtilsProfileAction(Redux
      .store!.state.utilsProfileState
      .copyWith(themeProvider: ThemeProvider(theme))));
}

void initUpdateTheme(ThemeProvider themeProvider) {
  Redux.store!.dispatch(SetUtilsProfileAction(Redux
      .store!.state.utilsProfileState
      .copyWith(themeProvider: themeProvider)));
}

void setUser(UserLoginModel? newUser) {
  StorageManager.storeUser(newUser);
  Redux.store!.dispatch(SetUtilsProfileAction(
      Redux.store!.state.utilsProfileState.copyWith(user: newUser)));
}

void setServer(String server) {
  StorageManager.storeServer(server);
  Redux.store!.dispatch(SetUtilsProfileAction(
      Redux.store!.state.utilsProfileState.copyWith(server: server)));
}

Future<bool> fetchIsUsersTokenValid() async {
  Redux.store!.dispatch(SetUtilsProfileAction(
      Redux.store!.state.utilsProfileState.copyWith(isLoading: true)));
  UserLoginModel? currentUser = Redux.store!.state.utilsProfileState.user;
  if (currentUser == null) {
    return false;
  }

  Map<String, String> verifyRequestHeaders = {
    'Accept': 'application/json',
    'X-Vera-API-Version': '1.6.0',
    'X-Vera-ClientIPAddr': '1',
    'X-Vera-Authorization': currentUser.authToken,
    'X-Vera-FceZarId': currentUser.user.positionList[0].id.toString(),
  };

  try {
    final response = await http
        .get(
            Uri.parse(
                '${Redux.store!.state.utilsProfileState.server}/ar/v1/token/verify'),
            headers: verifyRequestHeaders)
        .timeout(const Duration(seconds: 5), onTimeout: () {
      throw TimeoutException(
          'Odpověď serveru trvala příliš dlouho! Ujistěte se, že jste připojeni k internetu.');
    });
    if (response.statusCode == 400) {
      throw AssertionError("Token není platný!");
    } else if (response.statusCode == 200) {
      return true;
    } else {
      throw AssertionError("Neznámá chyba!");
    }
  } on SocketException {
    ErrorManager.showError("Chybné připojení k serveru!");
  } catch (error) {
    try {
      ErrorManager.showError(
          error.toString().split(":")[1].replaceAll(RegExp('"'), ''));
    } catch (e) {
      ErrorManager.showError(error.toString());
    }
  }
  Redux.store!.dispatch(SetUtilsProfileAction(
      Redux.store!.state.utilsProfileState.copyWith(isLoading: false)));
  return false;
}

Future<bool> loginUser(String login, String password, String domain) async {
  Redux.store!.dispatch(SetUtilsProfileAction(
      Redux.store!.state.utilsProfileState.copyWith(isLoading: true)));
  Map requestBody = {'login': login, 'password': password, 'domain': domain};
  try {
    final response = await http
        .post(
      Uri.parse('${Redux.store!.state.utilsProfileState.server}/ar/v1/login'),
      headers: requestHeaders,
      body: requestBody,
      encoding: Encoding.getByName('utf-8'),
    )
        .timeout(const Duration(seconds: 5), onTimeout: () {
      throw TimeoutException(
          'Odpověď serveru trvala příliš dlouho! Ujistěte se, že jste připojeni k internetu.');
    });
    if (response.statusCode == 401) {
      throw AssertionError("Nesprávné přihlašovací údaje!");
    } else if (response.statusCode == 412) {
      throw AssertionError("Chybná verze API!");
    } else if (response.statusCode == 200) {
      final jsonData = json.decode(utf8.decode(response.bodyBytes));
      final UserLoginModel user = UserLoginModel.fromJson(jsonData);
      Redux.store!.dispatch(SetUtilsProfileAction(Redux
          .store!.state.utilsProfileState
          .copyWith(user: user, isLoading: false)));
      StorageManager.storeUser(user);
      return true;
    } else {
      throw AssertionError("Neznámá chyba!");
    }
  } on SocketException {
    ErrorManager.showError("Chybné připojení k serveru!");
  } catch (error) {
    try {
      ErrorManager.showError(
          error.toString().split(":")[1].replaceAll(RegExp('"'), ''));
    } catch (e) {
      ErrorManager.showError(error.toString());
    }
  }
  Redux.store!.dispatch(SetUtilsProfileAction(
      Redux.store!.state.utilsProfileState.copyWith(isLoading: false)));
  return false;
}

Future<bool> logoutUser() async {
  Redux.store!.dispatch(SetUtilsProfileAction(
      Redux.store!.state.utilsProfileState.copyWith(isLoading: true)));
  UserLoginModel? currentUser = Redux.store!.state.utilsProfileState.user;
  if (currentUser == null) {
    return false;
  }
  Map<String, String> logoutRequestHeaders = {
    'X-Vera-API-Version': '1.6.0',
    'X-Vera-Authorization': currentUser.authToken,
  };
  try {
    final response = await http
        .post(
      Uri.parse('${Redux.store!.state.utilsProfileState.server}/ar/v1/logout'),
      headers: logoutRequestHeaders,
    )
        .timeout(const Duration(seconds: 5), onTimeout: () {
      throw TimeoutException(
          'Odpověď serveru trvala příliš dlouho! Ujistěte se, že jste připojeni k internetu.');
    });
    if (response.statusCode == 401) {
      throw AssertionError("Nesprávné přihlašovací údaje!");
    } else if (response.statusCode == 412) {
      throw AssertionError("Chybná verze API!");
    } else if (response.statusCode == 200) {
      ErrorManager.showSuccess("Byl jste odhlášen!");
      StorageManager.storeUser(null);
      Redux.store!.dispatch(SetUtilsProfileAction(
          Redux.store!.state.utilsProfileState.copyWith(isLoading: false)));
      return true;
    } else {
      throw AssertionError("Neznámá chyba!");
    }
  } on SocketException {
    ErrorManager.showError("Chybné připojení k serveru!");
  } catch (error) {
    try {
      ErrorManager.showError(
          error.toString().split(":")[1].replaceAll(RegExp('"'), ''));
    } catch (e) {
      ErrorManager.showError(error.toString());
    }
  }
  Redux.store!.dispatch(SetUtilsProfileAction(
      Redux.store!.state.utilsProfileState.copyWith(isLoading: false)));
  return false;
}
