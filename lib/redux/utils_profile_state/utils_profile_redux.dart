import 'package:vera/redux/utils_profile_state/utils_profile_actions.dart';
import 'package:vera/redux/utils_profile_state/utils_profile_state.dart';

utilsProfileReducer(UtilsProfileState prevState, dynamic action) {
  if (action is SetUtilsProfileAction) {
    final payload = action.utilsProfileState;
    return prevState.copyWith(
        themeProvider: payload.themeProvider,
        server: payload.server,
        user: payload.user,
        isLoading: payload.isLoading);
  }
  return prevState;
}
