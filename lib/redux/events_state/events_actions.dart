import 'package:intl/intl.dart';
import 'package:meta/meta.dart';
import 'package:vera/models/event_detail_model.dart';
import 'package:vera/models/resource_dial_model.dart';
import 'package:vera/models/user_dial_model.dart';
import 'package:vera/redux/events_state/events_state.dart';

import '../store.dart';

@immutable
class SetEventsAction {
  final EventsState eventsState;

  const SetEventsAction(this.eventsState);
}

class AddParticipantsToNewEventAction {
  final UserDialModel user;

  const AddParticipantsToNewEventAction(this.user);
}

class RemoveParticipantsToNewEventAction {
  final UserDialModel user;

  const RemoveParticipantsToNewEventAction(this.user);
}

class AddResourceToNewEventAction {
  final ResourceDialModel resourceDialModel;

  const AddResourceToNewEventAction(this.resourceDialModel);
}

class RemoveResourceToNewEventAction {
  final ResourceDialModel resourceDialModel;
  const RemoveResourceToNewEventAction(this.resourceDialModel);
}

class UpdateNewEventAction {
  final EventDetailModel eventDetailModel;
  const UpdateNewEventAction(this.eventDetailModel);
}

void updateTitle(String newTitle) {
  Redux.store!.dispatch(UpdateNewEventAction(
      Redux.store!.state.eventsState.newEvent!.copyWith(title: newTitle)));
}

void updatePlace(String newPlace) {
  Redux.store!.dispatch(UpdateNewEventAction(
      Redux.store!.state.eventsState.newEvent!.copyWith(place: newPlace)));
}

void updateNote(String newNote) {
  Redux.store!.dispatch(UpdateNewEventAction(
      Redux.store!.state.eventsState.newEvent!.copyWith(note: newNote)));
}

void toggleNotification() {
  Redux.store!.dispatch(UpdateNewEventAction(
      Redux.store!.state.eventsState.newEvent!.copyWith(
          notification:
              !Redux.store!.state.eventsState.newEvent!.notification)));
}

void toggleWholeDay() {
  Redux.store!.dispatch(UpdateNewEventAction(
      Redux.store!.state.eventsState.newEvent!.copyWith(
          isWholeDay: !Redux.store!.state.eventsState.newEvent!.isWholeDay)));
}

void toggleConfirm() {
  Redux.store!.dispatch(UpdateNewEventAction(Redux
      .store!.state.eventsState.newEvent!
      .copyWith(confirm: !Redux.store!.state.eventsState.newEvent!.confirm)));
}

void updateEventType(String newEventType) {
  Redux.store!.dispatch(UpdateNewEventAction(Redux
      .store!.state.eventsState.newEvent!
      .copyWith(eventType: newEventType)));
}

void updateDateSince(DateTime newDate) {
  Redux.store!.dispatch(UpdateNewEventAction(Redux
      .store!.state.eventsState.newEvent!
      .copyWith(dateSince: DateFormat('dd-MM-yyyy').format(newDate))));
}

void updateDateTo(DateTime newDate) {
  Redux.store!.dispatch(UpdateNewEventAction(Redux
      .store!.state.eventsState.newEvent!
      .copyWith(dateTo: DateFormat('dd-MM-yyyy').format(newDate))));
}

void updateTimeSince(DateTime newTime) {
  Redux.store!.dispatch(UpdateNewEventAction(Redux
      .store!.state.eventsState.newEvent!
      .copyWith(timeSince: DateFormat.Hm().format(newTime))));
}

void updateTimeTo(DateTime newTime) {
  Redux.store!.dispatch(UpdateNewEventAction(Redux
      .store!.state.eventsState.newEvent!
      .copyWith(timeTo: DateFormat.Hm().format(newTime))));
}

void toggleTrip() {
  Redux.store!.dispatch(UpdateNewEventAction(Redux
      .store!.state.eventsState.newEvent!
      .copyWith(trip: !Redux.store!.state.eventsState.newEvent!.trip)));
}

void addParticipantToNewEvent(UserDialModel user) {
  Redux.store!.dispatch(AddParticipantsToNewEventAction(user));
}

void removeParticipantToNewEvent(UserDialModel user) {
  Redux.store!.dispatch(RemoveParticipantsToNewEventAction(user));
}

void addResourceToNewEvent(ResourceDialModel resource) {
  Redux.store!.dispatch(AddResourceToNewEventAction(resource));
}

void removeResourceToNewEvent(ResourceDialModel resource) {
  Redux.store!.dispatch(RemoveResourceToNewEventAction(resource));
}
