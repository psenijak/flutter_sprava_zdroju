import 'package:vera/models/event_detail_model.dart';

class EventsState {
  final List<EventDetailModel>? eventList;
  final EventDetailModel? eventDetail;
  final EventDetailModel? newEvent;

  EventsState({this.eventList, this.eventDetail, this.newEvent, userDialList});

  factory EventsState.initial() {
    return EventsState(
        eventList: [], eventDetail: null, newEvent: EventDetailModel.initial());
  }

  EventsState copyWith(
      {List<EventDetailModel>? eventList,
      EventDetailModel? eventDetail,
      EventDetailModel? newEvent}) {
    return EventsState(
        eventList: eventList, eventDetail: eventDetail, newEvent: newEvent);
  }
}
