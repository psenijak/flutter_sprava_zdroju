import 'package:vera/redux/events_state/events_actions.dart';
import 'package:vera/redux/events_state/events_state.dart';

eventsReducer(EventsState prevState, dynamic action) {
  if (action is SetEventsAction) {
    final payload = action.eventsState;
    return prevState.copyWith(
        eventList: payload.eventList ?? prevState.eventList,
        eventDetail: payload.eventDetail ?? prevState.eventDetail,
        newEvent: payload.newEvent ?? prevState.newEvent);
  }
  if (action is UpdateNewEventAction) {
    return prevState.copyWith(
        eventList: prevState.eventList,
        eventDetail: prevState.eventDetail,
        newEvent: action.eventDetailModel);
  }
  if (action is AddParticipantsToNewEventAction) {
    return prevState.copyWith(
        newEvent: prevState.newEvent?.copyWith(
            participants: prevState.newEvent!.participants.contains(action.user)
                ? prevState.newEvent!.participants
                : [...prevState.newEvent!.participants, action.user]));
  }
  if (action is RemoveParticipantsToNewEventAction) {
    return prevState.copyWith(
        newEvent: prevState.newEvent?.copyWith(
            participants: prevState.newEvent!.participants.contains(action.user)
                ? [...prevState.newEvent!.participants..remove(action.user)]
                : prevState.newEvent!.participants));
  }
  if (action is AddResourceToNewEventAction) {
    return prevState.copyWith(
        newEvent: prevState.newEvent?.copyWith(
            resources:
                prevState.newEvent!.resources.contains(action.resourceDialModel)
                    ? prevState.newEvent!.resources
                    : [
                        ...prevState.newEvent!.resources,
                        action.resourceDialModel
                      ]));
  }
  if (action is RemoveResourceToNewEventAction) {
    return prevState.copyWith(
        newEvent: prevState.newEvent?.copyWith(
            resources:
                prevState.newEvent!.resources.contains(action.resourceDialModel)
                    ? [
                        ...prevState.newEvent!.resources
                          ..remove(action.resourceDialModel)
                      ]
                    : prevState.newEvent!.resources));
  }

  return prevState;
}
