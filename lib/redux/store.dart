import 'package:meta/meta.dart';
import 'package:redux/redux.dart';
import 'package:redux_thunk/redux_thunk.dart';
import 'package:vera/redux/dials_state/dials_reducer.dart';
import 'package:vera/redux/dials_state/dials_state.dart';
import 'package:vera/redux/events_state/events_state.dart';
import 'package:vera/redux/utils_profile_state/utils_profile_redux.dart';
import 'package:vera/redux/utils_profile_state/utils_profile_state.dart';

import 'events_state/events_reducer.dart';

AppState appReducer(AppState state, dynamic action) => AppState(
      dialsState: dialsReducer(state.dialsState, action),
      eventsState: eventsReducer(state.eventsState, action),
      utilsProfileState: utilsProfileReducer(state.utilsProfileState, action),
    );

@immutable
class AppState {
  final DialsState dialsState;
  final EventsState eventsState;
  final UtilsProfileState utilsProfileState;

  const AppState(
      {required this.dialsState,
      required this.eventsState,
      required this.utilsProfileState});

  AppState copyWith(
      {required DialsState dialsState,
      required EventsState eventsState,
      required UtilsProfileState utilsProfileState}) {
    return AppState(
        dialsState: dialsState,
        eventsState: eventsState,
        utilsProfileState: utilsProfileState);
  }
}

class Redux {
  static Store<AppState>? _store;

  static Store<AppState>? get store {
    if (_store == null) {
      throw Exception("store is not initialized");
    } else {
      return _store;
    }
  }

  static Future<void> init() async {
    final dialsStateInitial = DialsState.initial();
    final eventsStateInitial = EventsState.initial();
    final utilsProfileStateInitial = UtilsProfileState.initial();

    _store = Store<AppState>(
      appReducer,
      middleware: [thunkMiddleware],
      initialState: AppState(
          dialsState: dialsStateInitial,
          eventsState: eventsStateInitial,
          utilsProfileState: utilsProfileStateInitial),
    );
  }
}
